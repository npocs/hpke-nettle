/* TODO Insert license here */

#ifndef __HPKE_NETTLE_DHKEM_H__
#define __HPKE_NETTLE_DHKEM_H__

#include "hkdf.h"
#include "hpke_types.h"

#include <nettle/ecc.h>
#include <nettle/ecc-curve.h>
#include <nettle/curve25519.h>
#include <nettle/curve448.h>
#include <gmp.h>

#include <string.h>
#include <stdlib.h>
#include <sys/random.h>

int
encap (uint8_t *pkr, size_t pkr_len, struct suite_ctx *s_ctx);

int
decap (uint8_t *enc, size_t enc_len, uint8_t *pkr, size_t pkr_len,
       uint8_t *skr, size_t skr_len, struct suite_ctx *s_ctx);

int
encap_auth (uint8_t *pkr, size_t pkr_len, uint8_t *pks, size_t pks_len,
            uint8_t *sks, size_t sks_len, struct suite_ctx *s_ctx);

int
decap_auth (uint8_t *enc, size_t enc_len, uint8_t *pks, size_t pks_len,
            uint8_t *pkr, size_t pkr_len, uint8_t *skr, size_t skr_len,
            struct suite_ctx *s_ctx);

#ifdef TEST
int
encap_testable (char *pkR, char *pkE, char *skE, struct suite_ctx *s_ctx);

int
encap_auth_testable (char *pkR, char *pkE, char *skE, char *pkS,
                     char *skS, struct suite_ctx *s_ctx);
#endif /* TEST */

#endif /* __HPKE_NETTLE_DHKEM_H__ */
