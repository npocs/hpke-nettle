/* FIXME Insert license here */

#include "hpke_types.h"

void
suite_init (struct suite_ctx *c, const uint16_t dhkem, const uint16_t hkdf,
            const uint16_t aead)
{
  c->dhkem = dhkem;
  c->hkdf = hkdf;
  c->aead = aead;
  c->shared_secret = NULL;
  c->enc = NULL;
  c->enc_len = 0;

  if (dhkem == HPKE_DHKEM_P256)
    {
      c->Nsecret = 32;
      c->nsk = 32;
      c->ecc_order = HPKE_ECC_ORDER_P256;
      c->ecc = nettle_get_secp_256r1();
    }
  else if (dhkem == HPKE_DHKEM_X25519)
    {
      c->Nsecret = 32;
      c->nsk = CURVE25519_SIZE;
    }
  else if (dhkem == HPKE_DHKEM_P384)
    {
      c->Nsecret = 48;
      c->nsk = 48;
      c->ecc_order = HPKE_ECC_ORDER_P384;
      c->ecc = nettle_get_secp_384r1();
    }
  else if (dhkem == HPKE_DHKEM_P521)
    {
      c->Nsecret = 64;
      c->nsk = 66;
      c->ecc_order = HPKE_ECC_ORDER_P521;
      c->ecc = nettle_get_secp_521r1();
    }
  else if (dhkem == HPKE_DHKEM_X448)
    {
      c->Nsecret = 64;
      c->nsk = CURVE448_SIZE;
    }

  if (hkdf == HPKE_HKDF_SHA256)
    c->nh = 32;
  else if (hkdf == HPKE_HKDF_SHA384)
    c->nh = 48;
  else if (hkdf == HPKE_HKDF_SHA512)
    c->nh = 64;

  if (aead == HPKE_AEAD_AES_128_GCM)
    c->nk = 16;
  else if (aead == HPKE_AEAD_AES_256_GCM || aead == HPKE_AEAD_CHACHA20POLY1305)
    c->nk = 32;
  else if (aead == HPKE_AEAD_EXPORT_ONLY)
    c->nk = 0;
}

void
suite_clear (struct suite_ctx *c)
{
  if (c->shared_secret)
    {
      free(c->shared_secret);
      c->shared_secret = NULL;
    }
  if (c->enc)
    {
      free(c->enc);
      c->enc = NULL;
    }
}

int
enc_init (struct enc_ctx *e, uint8_t mode, uint8_t *info, size_t info_len,
    uint8_t *psk, size_t psk_len, uint8_t *psk_id, size_t psk_id_len)
{
  if(!info)
    return -1;

  e->mode = mode;
  e->info_len = info_len;
  e->info = calloc(info_len, 1);
  if(!e->info)
    return -1;
  memcpy(e->info, info, info_len);

  if (psk)
    {
      e->psk_len = psk_len;
      e->psk = calloc(psk_len, 1);
      if (!e->psk)
        {
          free(e->info);
          return -1;
      }
      memcpy(e->psk, psk, psk_len);
    }
  else
    {
      e->psk_len = 0;
      e->psk = NULL;
    }

  if (psk_id)
    {
      e->psk_id_len = psk_id_len;
      e->psk_id = calloc(psk_id_len, 1);
      if (!e->psk_id)
        {
          free(e->info);
          if (e->psk)
            free(e->psk);
          return -1;
        }
      memcpy(e->psk_id, psk_id, psk_id_len);
    }
  else
    {
      e->psk_id_len = 0;
      e->psk_id = NULL;
    }

  return 0;
}

void
enc_clear (struct enc_ctx *e)
{
  if (e->psk_id)
    free(e->psk_id);
  if (e->psk)
    free(e->psk);
  if (e->info)
    free(e->info);
}

int
context_init (struct context *c, uint8_t *key, uint8_t *base_nonce,
    uint8_t *exporter_secret, struct suite_ctx *s_ctx)
{
  if (exporter_secret == NULL)
    return -1;

  if (key)
    {
      c->key = calloc(s_ctx->nk, 1);
      if (!c->key)
        return -1;
      memcpy(c->key, key, s_ctx->nk);
    }
  else
      c->key = key;

  if (base_nonce)
    {
      c->base_nonce = calloc(HPKE_AEAD_NN, 1);
      if (!c->base_nonce)
        {
          if (c->key)
            free(c->key);
          return -1;
        }
      memcpy(c->base_nonce, base_nonce, HPKE_AEAD_NN);
    }
  else
    c->base_nonce = base_nonce;

  c->exporter_secret = calloc(s_ctx->nh, 1);
  if (!c->exporter_secret)
    {
      if (c->key)
        free(c->key);
      if (c->base_nonce)
        free(c->base_nonce);
      return -1;
    }

  memcpy(c->exporter_secret, exporter_secret, s_ctx->nh);
  c->seq_n = 0;

  c->s_ctx = *s_ctx;

  return 0;
}

void
context_clear (struct context *c)
{
  if (c->key)
    free(c->key);
  if (c->base_nonce)
    free(c->base_nonce);
  if (c->exporter_secret)
    free(c->exporter_secret);
  suite_clear(&(c->s_ctx));
}

int
i2osp (size_t n, uint8_t *s, const uint8_t l)
{
  if (n > pow(256, l))
    return -1;
  uint8_t index = l-1;
  uint8_t until = l <= sizeof(size_t) ? l : sizeof(size_t);

  for (uint8_t i = 0; i < until; i++)
    s[index-i] = (n >> (8*i)) & 0xFF;

  return 0;
}

void
os2ip (uint8_t *s, size_t *n, const uint8_t l)
{
  *n = 0;
  uint8_t index = l-1;
  for (uint8_t i = 0; i < l; i++) {
    *n |= s[index-i] << (8*i);
  }
}

int
ecc_point_to_octet (uint8_t **s, size_t *s_len, struct ecc_point *p,
    struct suite_ctx *s_ctx)
{
  if (!*p->p)
    {
      *s = calloc(1, 1);
      **s = 0x00;
      return 0;
    }
  size_t bit_size = (ecc_bit_size(s_ctx->ecc) + 7) / 8;

  mpz_t x,y;
  mpz_init(x);
  mpz_init(y);
  ecc_point_get(p, x, y);

  *s_len = 2*bit_size+1;
  *s = calloc(*s_len, 1);
  if (!*s)
    return -1;
  **s = 0x04;

  nettle_mpz_get_str_256(bit_size, (*s)+1, x);
  nettle_mpz_get_str_256(bit_size, (*s)+1+bit_size, y);

  mpz_clear(x);
  mpz_clear(y);

  return 0;
}

  int
octet_to_ecc_point (struct ecc_point *p, uint8_t *s, size_t s_len,
    struct suite_ctx *s_ctx)
{
  mpz_t x, y;
  int r;

  mpz_init(x);
  mpz_init(y);

  if (!s[0])
    r = ecc_point_set(p, x, y);
  else if (s[0] == 0x04)
    {
      unsigned bit_size = ((s_len-1)/2);
      nettle_mpz_set_str_256_u(x, bit_size, s+1);
      nettle_mpz_set_str_256_u(y, bit_size, s+1+bit_size);
      r = ecc_point_set(p, x, y);
    }

  mpz_clear(x);
  mpz_clear(y);

  return r;
}

int
octet_to_hex_string (char **dst, uint8_t *src, size_t src_len)
{
  *dst = calloc(src_len*2+1, 1);
  if(!*dst)
    return -1;

  size_t counter = 0;
  char *tmp = *dst;
  const char hex[16] = {'0', '1', '2', '3', '4', '5', '6', '7',
                        '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

  for (size_t i=0; i<src_len; i++)
    {
      tmp[counter++] = hex[(src[i] >> 4) & 0xF];
      tmp[counter++] = hex[(src[i] & 0x0F)];
    }
  tmp[counter] = '\0';

  return 0;
}

static int
get_hex_num (char hex)
{
  if (hex >= '0' && hex <= '9')
    return hex - '0';

  switch (hex)
    {
    case 'a':
      return 10;
    case 'b':
      return 11;
    case 'c':
      return 12;
    case 'd':
      return 13;
    case 'e':
      return 14;
    case 'f':
      return 15;
    default:
      return -1;
    }
}

int
hex_string_to_octet (uint8_t **dst, size_t *dst_len, char *src)
{
  *dst_len = strlen(src)/2;
  *dst = calloc(*dst_len, 1);
  if (!*dst)
    return -1;

  size_t counter = 0;
  uint8_t *tmp = *dst;
  size_t d_len = *dst_len;

  for (size_t i=0; i<d_len; i++)
    {
      tmp[i] = get_hex_num(src[counter++]) << 4;
      tmp[i] |= get_hex_num(src[counter++]);
    }

  return 0;
}

void
xor_octet_arr (uint8_t *dst, uint8_t *src0, uint8_t *src1, size_t len)
{
  for (size_t i=0; i < len; i++)
    dst[i] = src0[i] ^ src1[i];
}
