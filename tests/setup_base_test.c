#include <stdarg.h>
#include <setjmp.h>
#include <stddef.h>
#include <cmocka.h>
#include <jansson.h>

#include "../hpke.h"

void
test_sender(void **state) {
  json_t *root, *value;
  json_error_t err;
  uint8_t *info_o;
  size_t info_o_len, i;
  struct context ctx;

  root = json_load_file("../../tests/test-vectors.json", 0, &err);

  json_array_foreach(root, i, value) {
    int mode, kem_id, kdf_id, aead_id;
    const char *info, *pkr, *pke, *ske, *key, *base_nonce, *exporter_secret;
    mode = json_integer_value(json_object_get(value, "mode"));
    kem_id = json_integer_value(json_object_get(value, "kem_id"));
    kdf_id = json_integer_value(json_object_get(value, "kdf_id"));
    aead_id = json_integer_value(json_object_get(value, "aead_id"));
    info = json_string_value(json_object_get(value, "info"));
    pkr = json_string_value(json_object_get(value, "pkRm"));
    pke = json_string_value(json_object_get(value, "pkEm"));
    ske = json_string_value(json_object_get(value, "skEm"));
    key = json_string_value(json_object_get(value, "key"));
    base_nonce = json_string_value(json_object_get(value, "base_nonce"));
    exporter_secret = json_string_value(json_object_get(value, "exporter_secret"));
    hex_string_to_octet(&info_o, &info_o_len, (char*)info);

    if (mode == HPKE_MODE_BASE) 
      setup_base_s_testable((char*)pkr, (char*)pke, (char*)ske, info_o, info_o_len, kem_id, kdf_id, aead_id, &ctx);
    else if (mode == HPKE_MODE_PSK) {
      const char *psk, *psk_id;
      uint8_t *psk_o, *psk_id_o;
      size_t psk_o_len, psk_id_o_len;
      psk = json_string_value(json_object_get(value, "psk"));
      psk_id = json_string_value(json_object_get(value, "psk_id"));

      hex_string_to_octet(&psk_o, &psk_o_len, (char*)psk);
      hex_string_to_octet(&psk_id_o, &psk_id_o_len, (char*)psk_id);

      setup_psk_s_testable((char*)pkr, (char*)pke, (char*)ske, info_o, info_o_len, psk_o, psk_o_len, psk_id_o,
                           psk_id_o_len, kem_id, kdf_id, aead_id, &ctx);

      free(psk_o);
      free(psk_id_o);
    } else if (mode == HPKE_MODE_AUTH) {
      const char *pks, *sks;
      pks = json_string_value(json_object_get(value, "pkSm"));
      sks = json_string_value(json_object_get(value, "skSm"));

      setup_auth_s_testable((char*)pkr, (char*)pks, (char*)sks, (char*)pke, (char*)ske,
                            info_o, info_o_len, kem_id, kdf_id, aead_id, &ctx);
    } else if (mode == HPKE_MODE_AUTH_PSK) {
      const char *pks, *sks, *psk, *psk_id;
      uint8_t *psk_o, *psk_id_o;
      size_t psk_o_len, psk_id_o_len;

      psk = json_string_value(json_object_get(value, "psk"));
      psk_id = json_string_value(json_object_get(value, "psk_id"));
      pks = json_string_value(json_object_get(value, "pkSm"));
      sks = json_string_value(json_object_get(value, "skSm"));

      hex_string_to_octet(&psk_o, &psk_o_len, (char*)psk);
      hex_string_to_octet(&psk_id_o, &psk_id_o_len, (char*)psk_id);

      setup_auth_psk_s_testable((char*)pkr, (char*)pks, (char*)sks, (char*)pke, (char*)ske,
                                psk_o, psk_o_len, psk_id_o, psk_id_o_len,
                                info_o, info_o_len, kem_id, kdf_id, aead_id, &ctx);

      free(psk_o);
      free(psk_id_o);
    } else {
      free(info_o);
      continue;
    }

    char *key_out = NULL;
    char *base_nonce_out = NULL;
    char *exporter_s_out;
    if (aead_id != HPKE_AEAD_EXPORT_ONLY) {
      octet_to_hex_string(&key_out, ctx.key, ctx.s_ctx.nk);
      octet_to_hex_string(&base_nonce_out, ctx.base_nonce, HPKE_AEAD_NN);
    }
    octet_to_hex_string(&exporter_s_out, ctx.exporter_secret, ctx.s_ctx.nh);

    if (aead_id != HPKE_AEAD_EXPORT_ONLY) {
      assert_string_equal(key, key_out);
      assert_string_equal(base_nonce, base_nonce_out);
    }
    assert_string_equal(exporter_secret, exporter_s_out);


    free(info_o);
    if (key_out)
      free(key_out);
    if (base_nonce_out)
      free(base_nonce_out);
    free(exporter_s_out);
    context_clear(&ctx);
  }

  json_decref(root);
}

void
test_receiver(void **state) {
  json_t *root, *value;
  json_error_t err;
  size_t i;
  uint8_t *pkr_o, *skr_o, *pke_o, *info_o;
  size_t pkr_o_len, skr_o_len, pke_o_len, info_o_len;
  struct context ctx;

  root = json_load_file("../../tests/test-vectors.json", 0, &err);

  json_array_foreach(root, i, value) {
    int mode, kem_id, kdf_id, aead_id;
    const char *pkr, *skr, *pke, *info, *key, *base_nonce, *exporter_secret;
    mode = json_integer_value(json_object_get(value, "mode"));
    kem_id = json_integer_value(json_object_get(value, "kem_id"));
    kdf_id = json_integer_value(json_object_get(value, "kdf_id"));
    aead_id = json_integer_value(json_object_get(value, "aead_id"));
    info = json_string_value(json_object_get(value, "info"));
    pkr = json_string_value(json_object_get(value, "pkRm"));
    skr = json_string_value(json_object_get(value, "skRm"));
    pke = json_string_value(json_object_get(value, "pkEm"));
    key = json_string_value(json_object_get(value, "key"));
    base_nonce = json_string_value(json_object_get(value, "base_nonce"));
    exporter_secret = json_string_value(json_object_get(value, "exporter_secret"));

    hex_string_to_octet(&pkr_o, &pkr_o_len, (char*)pkr);
    hex_string_to_octet(&skr_o, &skr_o_len, (char*)skr);
    hex_string_to_octet(&pke_o, &pke_o_len, (char*)pke);
    hex_string_to_octet(&info_o, &info_o_len, (char*)info);

    if (mode == HPKE_MODE_BASE)
      setup_base_r(pke_o, pke_o_len, pkr_o, pkr_o_len, skr_o, skr_o_len,
                   info_o, info_o_len, kem_id, kdf_id, aead_id, &ctx);
    else if (mode == HPKE_MODE_PSK) {
      const char *psk, *psk_id;
      uint8_t *psk_o, *psk_id_o;
      size_t psk_o_len, psk_id_o_len;
      psk = json_string_value(json_object_get(value, "psk"));
      psk_id = json_string_value(json_object_get(value, "psk_id"));

      hex_string_to_octet(&psk_o, &psk_o_len, (char*)psk);
      hex_string_to_octet(&psk_id_o, &psk_id_o_len, (char*)psk_id);

      setup_psk_r(pke_o, pke_o_len, pkr_o, pkr_o_len, skr_o, skr_o_len,
                  info_o, info_o_len, psk_o, psk_o_len, psk_id_o,
                  psk_id_o_len, kem_id, kdf_id, aead_id, &ctx);

      free(psk_o);
      free(psk_id_o);
    } else if (mode == HPKE_MODE_AUTH) {
      const char *pks;
      uint8_t *pks_o;
      size_t pks_o_len;

      pks = json_string_value(json_object_get(value, "pkSm"));
      hex_string_to_octet(&pks_o, &pks_o_len, (char*)pks);

      setup_auth_r(pke_o, pke_o_len, pks_o, pks_o_len, pkr_o, pkr_o_len,
                   skr_o, skr_o_len, info_o, info_o_len,
                   kem_id, kdf_id, aead_id, &ctx);

      free(pks_o);
    } else if (mode == HPKE_MODE_AUTH_PSK) {
      const char *pks;
      uint8_t *pks_o;
      size_t pks_o_len;
      const char *psk, *psk_id;
      uint8_t *psk_o, *psk_id_o;
      size_t psk_o_len, psk_id_o_len;

      psk = json_string_value(json_object_get(value, "psk"));
      psk_id = json_string_value(json_object_get(value, "psk_id"));
      pks = json_string_value(json_object_get(value, "pkSm"));

      hex_string_to_octet(&psk_o, &psk_o_len, (char*)psk);
      hex_string_to_octet(&psk_id_o, &psk_id_o_len, (char*)psk_id);
      hex_string_to_octet(&pks_o, &pks_o_len, (char*)pks);

      setup_auth_psk_r(pke_o, pke_o_len, pks_o, pks_o_len, pkr_o, pkr_o_len,
                       skr_o, skr_o_len, psk_o, psk_o_len,
                       psk_id_o, psk_id_o_len,info_o, info_o_len,
                       kem_id, kdf_id, aead_id, &ctx);

      free(pks_o);
      free(psk_o);
      free(psk_id_o);
    } else {
      free(pkr_o);
      free(pke_o);
      free(skr_o);
      free(info_o);
      continue;
    }

    char *key_out = NULL;
    char *base_nonce_out = NULL;
    char *exporter_s_out;
    if (aead_id != HPKE_AEAD_EXPORT_ONLY) {
      octet_to_hex_string(&key_out, ctx.key, ctx.s_ctx.nk);
      octet_to_hex_string(&base_nonce_out, ctx.base_nonce, HPKE_AEAD_NN);
    }
    octet_to_hex_string(&exporter_s_out, ctx.exporter_secret, ctx.s_ctx.nh);

    if (aead_id != HPKE_AEAD_EXPORT_ONLY) {
      assert_string_equal(key, key_out);
      assert_string_equal(base_nonce, base_nonce_out);
    }
    assert_string_equal(exporter_secret, exporter_s_out);

    free(pkr_o);
    free(pke_o);
    free(skr_o);
    free(info_o);
    if (key_out)
      free(key_out);
    if (base_nonce_out)
      free(base_nonce_out);
    free(exporter_s_out);
    context_clear(&ctx);
  }

  json_decref(root);
}

int
main(void) {
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(test_sender),
    cmocka_unit_test(test_receiver),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
