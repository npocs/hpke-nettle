#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <nettle/ecdsa.h>
#include <nettle/ecc.h>
#include <nettle/ecc-curve.h>
#include <nettle/bignum.h>
#include <gmp.h>
#include <string.h>
#include <cmocka.h>
#include <jansson.h>

#include "../hkdf.h"
#include "../dhkem.h"

#include <nettle/knuth-lfib.h>

#include <jansson.h>

void
test_encap(void **state) {
  struct suite_ctx c;
  json_error_t err;
  size_t i;
  json_t *value;

  json_t *root = json_load_file("../../tests/test-vectors.json", 0, &err);

  json_array_foreach(root, i, value) {
    int mode, kem_id, kdf_id, aead_id;
    mode = json_integer_value(json_object_get(value, "mode"));
    //if (mode != HPKE_MODE_BASE && mode != HPKE_MODE_PSK) continue; //for now just basic&psk mode
    kem_id = json_integer_value(json_object_get(value, "kem_id"));
    kdf_id = json_integer_value(json_object_get(value, "kdf_id"));
    aead_id = json_integer_value(json_object_get(value, "aead_id"));

    suite_init(&c, kem_id, kdf_id, aead_id);

    const char *pkr = json_string_value(json_object_get(value, "pkRm"));
    const char *pke = json_string_value(json_object_get(value, "pkEm"));
    const char *ske = json_string_value(json_object_get(value, "skEm"));
    const char *sh_s = json_string_value(json_object_get(value, "shared_secret"));

    if (mode == HPKE_MODE_BASE || mode == HPKE_MODE_PSK)
      encap_testable((char*)pkr, (char*)pke, (char*)ske, &c);
    else {
      const char *pks = json_string_value(json_object_get(value, "pkSm"));
      const char *sks = json_string_value(json_object_get(value, "skSm"));
      encap_auth_testable((char*)pkr, (char*)pke, (char*)ske,
                          (char*)pks, (char*)sks, &c);
    }

    char *shared_s_out;
    octet_to_hex_string(&shared_s_out, c.shared_secret, c.Nsecret);
    assert_string_equal(sh_s, shared_s_out);

    free(shared_s_out);
    suite_clear(&c);
  }

  json_decref(root);
}

void
test_decap(void **state) {
  struct suite_ctx c;
  json_error_t err;
  size_t i;
  json_t *value;

  json_t *root = json_load_file("../../tests/test-vectors.json", 0, &err);

  json_array_foreach(root, i, value) {
    int mode, kem_id, kdf_id, aead_id;
    mode = json_integer_value(json_object_get(value, "mode"));
    if (mode != HPKE_MODE_BASE && mode != HPKE_MODE_PSK) continue; //for now just basic&psk mode
    kem_id = json_integer_value(json_object_get(value, "kem_id"));
    kdf_id = json_integer_value(json_object_get(value, "kdf_id"));
    aead_id = json_integer_value(json_object_get(value, "aead_id"));

    suite_init(&c, kem_id, kdf_id, aead_id);

    const char *pkr = json_string_value(json_object_get(value, "pkRm"));
    const char *skr = json_string_value(json_object_get(value, "skRm"));
    const char *pke = json_string_value(json_object_get(value, "pkEm"));
    const char *sh_s = json_string_value(json_object_get(value, "shared_secret"));

    char *shared_s_out;
    uint8_t *pkrm_oct, *skrm_oct, *pke_oct;
    size_t pkrm_oct_len, skrm_oct_len, pke_oct_len;

    hex_string_to_octet(&pkrm_oct, &pkrm_oct_len, (char*)pkr);
    hex_string_to_octet(&skrm_oct, &skrm_oct_len, (char*)skr);
    hex_string_to_octet(&pke_oct, &pke_oct_len, (char*)pke);

    decap(pke_oct, pke_oct_len, pkrm_oct, pkrm_oct_len, skrm_oct, skrm_oct_len, &c);

    octet_to_hex_string(&shared_s_out, c.shared_secret, c.Nsecret);
    assert_string_equal((char*)sh_s, shared_s_out);

    free(shared_s_out);
    free(pkrm_oct);
    free(skrm_oct);
    free(pke_oct);
    suite_clear(&c);
  }

  json_decref(root);
}

void
test_encap_decap(void **state) {
  struct suite_ctx s_ctx;
  uint8_t *shared_sec;
  uint8_t *enc;
  uint8_t *pub_ser;
  size_t shared_sec_len;
  size_t enc_len;
  size_t pub_ser_len;
  json_error_t err;
  size_t i;
  json_t *value;
  uint8_t *skr_oct;
  size_t skr_oct_len;

  json_t *root = json_load_file("../../tests/test-vectors.json", 0, &err);

  json_array_foreach(root, i, value) {
    int mode, kem_id, kdf_id, aead_id;
    mode = json_integer_value(json_object_get(value, "mode"));
    if (mode != HPKE_MODE_BASE && mode != HPKE_MODE_PSK) continue; //for now just basic&psk mode
    kem_id = json_integer_value(json_object_get(value, "kem_id"));
    kdf_id = json_integer_value(json_object_get(value, "kdf_id"));
    aead_id = json_integer_value(json_object_get(value, "aead_id"));

    suite_init(&s_ctx, kem_id, kdf_id, aead_id);

    uint8_t *ikm = calloc(s_ctx.nsk, 1);
    getrandom((void*)ikm, s_ctx.nsk, 0);

    if (kem_id == HPKE_DHKEM_P256
        || kem_id == HPKE_DHKEM_P384
        || kem_id == HPKE_DHKEM_P521) {
      struct ecc_point pkr;
      struct ecc_scalar skr;
      ecc_point_init(&pkr, s_ctx.ecc);
      ecc_scalar_init(&skr, s_ctx.ecc);

      derive_key_pair_p(ikm, s_ctx.nsk, &s_ctx, &skr, &pkr);

      ecc_point_to_octet(&pub_ser, &pub_ser_len, &pkr, &s_ctx);

      mpz_t z;
      mpz_init(z);

      ecc_scalar_get(&skr, z);
      skr_oct_len = nettle_mpz_sizeinbase_256_u(z);
      skr_oct = calloc(skr_oct_len, 1);
      nettle_mpz_get_str_256(skr_oct_len, skr_oct, z);
      mpz_clear(z);
      ecc_point_clear(&pkr);
      ecc_scalar_clear(&skr);
    } else {
      derive_key_pair_x(ikm, s_ctx.nsk, &s_ctx, &skr_oct, &pub_ser);
      pub_ser_len = s_ctx.nsk;
      skr_oct_len = s_ctx.nsk;
    }

    free(ikm);

    encap(pub_ser, pub_ser_len, &s_ctx);
    shared_sec_len = s_ctx.Nsecret;
    shared_sec = calloc(shared_sec_len, 1);
    memcpy(shared_sec, s_ctx.shared_secret, shared_sec_len);
    enc = s_ctx.enc;
    enc_len = s_ctx.enc_len;

    free(s_ctx.shared_secret);

    decap(enc, enc_len, pub_ser, pub_ser_len, skr_oct, skr_oct_len, &s_ctx);
    shared_sec_len = s_ctx.Nsecret;
    uint8_t *tmp_shared = calloc(shared_sec_len, 1);
    memcpy(tmp_shared, s_ctx.shared_secret, shared_sec_len);
    assert_memory_equal(shared_sec, tmp_shared, shared_sec_len);

    free(shared_sec);
    free(tmp_shared);
    free(skr_oct);
    free(pub_ser);
    suite_clear(&s_ctx);
  }

  json_decref(root);
}

int
main (void) {
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(test_encap),
    cmocka_unit_test(test_decap),
    cmocka_unit_test(test_encap_decap),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
