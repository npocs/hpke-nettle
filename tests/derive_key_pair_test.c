#include "../hkdf.h"

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <gmp.h>
#include <cmocka.h>
#include <jansson.h>

static void
test_p (char *ikm, char *skm, char *pkm, struct suite_ctx *s_ctx) {
  uint8_t *ikm_o;
  size_t ikm_o_len;
  struct ecc_scalar sk;
  struct ecc_point pk;

  ecc_scalar_init(&sk, s_ctx->ecc);
  ecc_point_init(&pk, s_ctx->ecc);

  hex_string_to_octet(&ikm_o, &ikm_o_len, ikm);

  derive_key_pair_p(ikm_o, ikm_o_len, s_ctx, &sk, &pk);

  uint8_t *pk_der;
  size_t pk_der_len;
  char *pk_der_hex;

  ecc_point_to_octet(&pk_der, &pk_der_len, &pk, s_ctx);
  octet_to_hex_string(&pk_der_hex, pk_der, pk_der_len);

  mpz_t s;
  uint8_t *sk_der;
  char *sk_der_hex;

  mpz_init(s);
  ecc_scalar_get(&sk, s);
  sk_der = calloc(s_ctx->nsk, 1);
  nettle_mpz_get_str_256(s_ctx->nsk, sk_der, s); 
  octet_to_hex_string(&sk_der_hex, sk_der, s_ctx->nsk);

  assert_string_equal(pkm, pk_der_hex);
  assert_string_equal(skm, sk_der_hex);

  free(ikm_o);
  ecc_scalar_clear(&sk);
  ecc_point_clear(&pk);
  mpz_clear(s);
  free(pk_der);
  free(sk_der);
  free(pk_der_hex);
  free(sk_der_hex);
}

static void
test_x (char *ikm, char *skm, char *pkm, struct suite_ctx *s_ctx) {
  uint8_t *ikm_o;
  size_t ikm_o_len;

  hex_string_to_octet(&ikm_o, &ikm_o_len, ikm);

  uint8_t *sk;
  uint8_t *pk;

  derive_key_pair_x(ikm_o, ikm_o_len, s_ctx, &sk, &pk);

  char *sk_hex;
  char *pk_hex;

  octet_to_hex_string(&sk_hex, sk, s_ctx->nsk);
  octet_to_hex_string(&pk_hex, pk, s_ctx->nsk);

  assert_string_equal(skm, sk_hex);
  assert_string_equal(pkm, pk_hex);

  free(ikm_o);
  free(sk);
  free(pk);
  free(sk_hex);
  free(pk_hex);
}

void
test(void **state) {
  json_t *root, *value;
  json_error_t err;
  size_t i;
  struct suite_ctx s_ctx;

  root = json_load_file("../../tests/test-vectors.json", 0, &err);

  json_array_foreach(root, i, value) {
    int kem_id, kdf_id, aead_id;
    const char *ikmR, *skmR, *pkmR, *ikmE, *skmE, *pkmE;
    kem_id = json_integer_value(json_object_get(value, "kem_id"));
    kdf_id = json_integer_value(json_object_get(value, "kdf_id"));
    aead_id = json_integer_value(json_object_get(value, "aead_id"));

    ikmR = json_string_value(json_object_get(value, "ikmR"));
    skmR = json_string_value(json_object_get(value, "skRm"));
    pkmR = json_string_value(json_object_get(value, "pkRm"));
    ikmE = json_string_value(json_object_get(value, "ikmE"));
    skmE = json_string_value(json_object_get(value, "skEm"));
    pkmE = json_string_value(json_object_get(value, "pkEm"));

    suite_init(&s_ctx, kem_id, kdf_id, aead_id);

    if (kem_id == HPKE_DHKEM_P256
        || kem_id == HPKE_DHKEM_P384
        || kem_id == HPKE_DHKEM_P521) {
      test_p((char*)ikmR, (char*)skmR, (char*)pkmR, &s_ctx);
      test_p((char*)ikmE, (char*)skmE, (char*)pkmE, &s_ctx);
    } else {
      test_x((char*)ikmR, (char*)skmR, (char*)pkmR, &s_ctx);
      test_x((char*)ikmE, (char*)skmE, (char*)pkmE, &s_ctx);
    }
    suite_clear(&s_ctx);
  }

  json_decref(root);
}

int
main () {
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(test),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
