#include "../hpke.h"
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <jansson.h>

void
test_seal_open (void **state) {
  json_t *root, *value;
  json_error_t err;
  size_t i;

  root = json_load_file("../../tests/test-vectors.json", 0, &err);

  json_array_foreach(root, i, value) {
    int kem_id, kdf_id, aead_id;
    const char *key, *base_nonce, *exporter_secret, *pt;
    struct suite_ctx s;
    struct context ctx;
    uint8_t *key_o, *b_n_o, *e_s_o, *pt_o;
    size_t key_o_len, b_n_o_len, e_s_o_len, pt_o_len;

    kem_id = json_integer_value(json_object_get(value, "kem_id"));
    kdf_id = json_integer_value(json_object_get(value, "kdf_id"));
    aead_id = json_integer_value(json_object_get(value, "aead_id"));

    if (aead_id == HPKE_AEAD_EXPORT_ONLY) continue;

    suite_init(&s, kem_id, kdf_id, aead_id);

    key = json_string_value(json_object_get(value, "key"));
    base_nonce = json_string_value(json_object_get(value, "base_nonce"));
    exporter_secret = json_string_value(json_object_get(value, "exporter_secret"));

    hex_string_to_octet(&key_o, &key_o_len, (char*)key);
    hex_string_to_octet(&b_n_o, &b_n_o_len, (char*)base_nonce);
    hex_string_to_octet(&e_s_o, &e_s_o_len, (char*)exporter_secret);

    context_init(&ctx, key_o, b_n_o, e_s_o, &s);

    pt = json_string_value(json_object_get(json_array_get(json_object_get(value, "encryptions"), 0), "plaintext"));

    hex_string_to_octet(&pt_o, &pt_o_len, (char*)pt);

    json_t *enc_arr, *enc_item;
    size_t enc_i;

    enc_arr = json_object_get(value, "encryptions");

    json_array_foreach(enc_arr, enc_i, enc_item) {
      const char *aad, *ct, *nonce;
      uint8_t *aad_o, *ct_o, *base_nonce_o, *pt_open;
      size_t aad_o_len, ct_o_len, base_nonce_o_len, pt_open_len;
      char *ct_out, *pt_out, *nonce_hex;
      uint8_t *seq_octet = calloc(HPKE_AEAD_NN, 1);
      uint8_t *nonce_o = calloc(HPKE_AEAD_NN, 1);

      aad = json_string_value(json_object_get(enc_item, "aad"));
      ct = json_string_value(json_object_get(enc_item, "ciphertext"));
      nonce = json_string_value(json_object_get(enc_item, "nonce"));

      hex_string_to_octet(&aad_o, &aad_o_len, (char*)aad);
      hex_string_to_octet(&base_nonce_o, &base_nonce_o_len, (char*)base_nonce);

      //test nonce counting
      i2osp(enc_i, seq_octet, HPKE_AEAD_NN);
      xor_octet_arr(nonce_o, base_nonce_o, seq_octet, HPKE_AEAD_NN);
      octet_to_hex_string(&nonce_hex, nonce_o, HPKE_AEAD_NN);
      assert_string_equal(nonce, nonce_hex);
      free(seq_octet);
      free(nonce_o);
      free(nonce_hex);

      hpke_seal(&ctx, aad_o, aad_o_len, pt_o, pt_o_len, &ct_o, &ct_o_len);

      octet_to_hex_string(&ct_out, ct_o, ct_o_len);
      assert_string_equal(ct, ct_out);

      --ctx.seq_n;

      hpke_open(&ctx, aad_o, aad_o_len, ct_o, ct_o_len, &pt_open, &pt_open_len);

      octet_to_hex_string(&pt_out, pt_open, pt_open_len);
      assert_string_equal(pt, pt_out);

      free(ct_o);
      free(ct_out);
      free(aad_o);
      free(base_nonce_o);
      free(pt_open);
      free(pt_out);
    }

    free(pt_o);
    free(key_o);
    free(b_n_o);
    free(e_s_o);
    context_clear(&ctx);
  }

  json_decref(root);
}

void
test_export(void **state) {
  json_t *root, *value;
  json_error_t err;
  size_t i;
  uint8_t *exp_con, *exp_out;
  size_t exp_con_len;
  char *exp_out_hex;

  root = json_load_file("../../tests/test-vectors.json", 0, &err);

  json_array_foreach(root, i, value) {
    int kem_id, kdf_id, aead_id, exp_len;
    const char *exporter_secret, *exporter_context, *exported_value;
    struct suite_ctx s;
    struct context ctx;
    uint8_t *e_s_o;
    size_t e_s_o_len;

    kem_id = json_integer_value(json_object_get(value, "kem_id"));
    kdf_id = json_integer_value(json_object_get(value, "kdf_id"));
    aead_id = json_integer_value(json_object_get(value, "aead_id"));

    suite_init(&s, kem_id, kdf_id, aead_id);

    exporter_secret = json_string_value(json_object_get(value, "exporter_secret"));

    hex_string_to_octet(&e_s_o, &e_s_o_len, (char*)exporter_secret);

    context_init(&ctx, NULL, NULL, e_s_o, &s);

    json_t *exp, *exp_val;
    size_t exp_i;

    exp = json_object_get(value, "exports");

    json_array_foreach(exp, exp_i, exp_val) {
      exporter_context = json_string_value(json_object_get(exp_val, "exporter_context"));
      exp_len = json_integer_value(json_object_get(exp_val, "L"));
      exported_value = json_string_value(json_object_get(exp_val, "exported_value"));

      hex_string_to_octet(&exp_con, &exp_con_len, (char*)exporter_context);

      exp_out = export_secret(&ctx, exp_con, exp_con_len, exp_len);

      octet_to_hex_string(&exp_out_hex, exp_out, exp_len);
      assert_string_equal(exported_value, exp_out_hex);

      free(exp_con);
      free(exp_out);
      free(exp_out_hex);
    }

    free(e_s_o);
    suite_clear(&s);
    context_clear(&ctx);
  }

  json_decref(root);
}

int
main (void) {
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(test_seal_open),
    cmocka_unit_test(test_export),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
