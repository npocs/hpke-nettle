#include <assert.h>
#include <jansson.h>

#include <stdlib.h>

#include "../hpke.h"
#include "../hpke_types.h"

static int
test (char *info, char *shared_secret, char *key, char *base_nonce, char *exporter_secret, struct suite_ctx *s,
      char *psk, char *psk_id, uint8_t mode) {
	uint8_t *info_octet;
	uint8_t *sh_sec_o;
        uint8_t *psk_o = NULL;
        uint8_t *psk_id_o = NULL;
	size_t info_octet_len;
	size_t sh_sec_o_len;
        size_t psk_o_len = 0;
        size_t psk_id_o_len = 0;
	struct enc_ctx e;
	struct context ctx;

	hex_string_to_octet(&info_octet, &info_octet_len, info);
	hex_string_to_octet(&sh_sec_o, &sh_sec_o_len, shared_secret);
        if (psk)
          hex_string_to_octet(&psk_o, &psk_o_len, psk);
        if (psk_id)
          hex_string_to_octet(&psk_id_o, &psk_id_o_len, psk_id);

	enc_init(&e, mode, info_octet, info_octet_len, psk_o, psk_o_len, psk_id_o, psk_id_o_len);

	s->shared_secret = sh_sec_o;

	if(key_schedule(&e, s, &ctx, mode) < 0)
		return -1;

	char *k_out = NULL;
	char *bn_out = NULL;
	char *es_out;

        if (s->aead != HPKE_AEAD_EXPORT_ONLY) {
          octet_to_hex_string(&k_out, ctx.key, ctx.s_ctx.nk);
          octet_to_hex_string(&bn_out, ctx.base_nonce, HPKE_AEAD_NN);
        }
	octet_to_hex_string(&es_out, ctx.exporter_secret, ctx.s_ctx.nh);

        if (s->aead != HPKE_AEAD_EXPORT_ONLY) {
          assert(strcmp(k_out, key) == 0);
          assert(strcmp(bn_out, base_nonce) == 0);
        }
	assert(strcmp(es_out, exporter_secret) == 0);

	free(info_octet);
        if (k_out)
          free(k_out);
        if (bn_out)
          free(bn_out);
        if (psk_o)
          free(psk_o);
        if (psk_id_o)
          free(psk_id_o);
	free(es_out);
	enc_clear(&e);
	context_clear(&ctx);

	return 0;
}

/* Test vectors @
 * https://github.com/cfrg/draft-irtf-cfrg-hpke/blob/779d0285fe0e9407abb549ba0104e831d9677164/test-vectors.json */
int main() {
	int r = 0;
	struct suite_ctx s;

        json_t *root;
        json_error_t err;

        root = json_load_file("../../tests/test-vectors.json", 0, &err);

        size_t i;
        json_t *value;

        json_array_foreach(root, i, value) {
          int mode, kem_id, kdf_id, aead_id;
          mode = json_integer_value(json_object_get(value, "mode"));
          if (mode != HPKE_MODE_BASE && mode != HPKE_MODE_PSK) continue; //for now just basic&psk mode
          kem_id = json_integer_value(json_object_get(value, "kem_id"));
          kdf_id = json_integer_value(json_object_get(value, "kdf_id"));
          aead_id = json_integer_value(json_object_get(value, "aead_id"));

          suite_init(&s, kem_id, kdf_id, aead_id);

          const char *info = json_string_value(json_object_get(value, "info"));
          const char *shared_secret = json_string_value(json_object_get(value, "shared_secret"));
          const char *key = json_string_value(json_object_get(value, "key"));
          const char *base_nonce = json_string_value(json_object_get(value, "base_nonce"));
          const char *exporter_secret = json_string_value(json_object_get(value, "exporter_secret"));
          const char *psk = json_string_value(json_object_get(value, "psk"));
          const char *psk_id = json_string_value(json_object_get(value, "psk_id"));

          r = test((char*)info, (char*)shared_secret, (char*)key, (char*)base_nonce, (char*)exporter_secret, &s,
                   (char*)psk, (char*)psk_id, mode);
          if (r)
            goto out;
        }

out:
        json_decref(root);

	return r;
}
