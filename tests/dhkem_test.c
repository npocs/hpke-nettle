#include <stdio.h>

#include "../hkdf.h"

void print_uint8_array(uint8_t *arr, size_t arr_len) {
	for(size_t i=0; i<arr_len-1; i++) {
		printf("%x", arr[i]);
	}
	printf("%x\n ", arr[arr_len-1]);
}

int main() {
	// for now, the context is created here
	struct suite_ctx s_ctx;
	uint8_t *result_extract;
	uint8_t *result_expand;
	uint8_t salt[4] = {'s','a','l','t'};
	char *label = "label";
	uint8_t ikm[3] = {'i','k','m'};
	uint8_t info[4] = {'i','n','f','o'};
	uint16_t dhkem[5] = {HPKE_DHKEM_P256, HPKE_DHKEM_P384, HPKE_DHKEM_P521, HPKE_DHKEM_X25519, HPKE_DHKEM_X448};
	uint16_t hkdf[3] = {HPKE_HKDF_SHA256, HPKE_HKDF_SHA384, HPKE_HKDF_SHA512};
	uint16_t aead[3] = {HPKE_AEAD_AES_128_GCM, HPKE_AEAD_AES_256_GCM, HPKE_AEAD_CHACHA20POLY1305};

	for (uint8_t i=0; i<5; i++) {
		suite_init(&s_ctx, dhkem[i], hkdf[0], aead[0]);
		result_extract = labeled_extract_kem(salt, 4, label, ikm, 3, &s_ctx);
		printf("KEM kem: 0x%xa\n\t result_extract: ", dhkem[i]);
		print_uint8_array(result_extract, s_ctx.digest_size);
		result_expand = labeled_expand_kem(result_extract, s_ctx.digest_size, label, info, 4, 64, &s_ctx);
		printf("\t result_expand: ");
		print_uint8_array(result_expand, 64);
		
		suite_clear(&s_ctx);
		free(result_extract);
		free(result_expand);

		for (uint8_t j=0; j<3; j++) {
			for (uint8_t k=0; k<3; k++) {
				suite_init(&s_ctx, dhkem[i], hkdf[j], aead[k]);
				result_extract = labeled_extract_hpke(salt, 4, label, ikm, 3, &s_ctx);
				printf("HPKE kem: 0x%x, kdf: 0x%x, aead: 0x%x\n\t result_extract: ", dhkem[i], hkdf[j], aead[k]);
				print_uint8_array(result_extract, s_ctx.digest_size);
				result_expand = labeled_expand_hpke(result_extract, s_ctx.digest_size, label, info, 4, 64, &s_ctx);
				printf("\t result_expand: ");
				print_uint8_array(result_expand, 64);
				
				suite_clear(&s_ctx);
				free(result_extract);
				free(result_expand);
			}
		}
	}

	return 0;
}
