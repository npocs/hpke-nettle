#include "../hpke_types.h"

#include <nettle/knuth-lfib.h>
#include <nettle/ecdsa.h>
#include <nettle/ecc.h>
#include <nettle/ecc-curve.h>
#include <assert.h>
#include <stdlib.h>

#include <stdio.h>

int
main() {
	struct ecc_point pkr;
	struct ecc_point out;
	struct ecc_scalar skr;
	struct knuth_lfib_ctx rctx;
	knuth_lfib_init(&rctx, 8939);
	uint8_t *t;
	size_t t_len;
	int r = 0;
	struct suite_ctx s_ctx;

	suite_init(&s_ctx, HPKE_DHKEM_P256, HPKE_HKDF_SHA256, HPKE_AEAD_AES_128_GCM);

	ecc_point_init(&pkr, s_ctx.ecc);
	ecc_point_init(&out, s_ctx.ecc);
	ecc_scalar_init(&skr, s_ctx.ecc);

	//commented out because of different outputs on different environments
	//point is 0
	/*if(ecc_point_to_octet(&t, &t_len, &pkr, &s_ctx) != 0)
		return -1;

	assert(t[0] == 0x0);

	free(t);*/

	ecdsa_generate_keypair(&pkr, &skr, &rctx, (nettle_random_func*) knuth_lfib_random);

	//point is real value
	if(ecc_point_to_octet(&t, &t_len, &pkr, &s_ctx) != 0) {
		r = -1;
		goto out;
	}

	assert(t[0] == 0x04);

	octet_to_ecc_point(&out, t, t_len, &s_ctx);

	assert(*out.p == *pkr.p);

	char *s;
	if(octet_to_hex_string(&s, t, t_len) < 0) {
		r = -1;
		goto out;
	}

	uint8_t *ret;
	size_t ret_len;

	if(hex_string_to_octet(&ret, &ret_len, s) < 0) {
		r = -1;
		goto out;
	}

	assert(ret_len == t_len);

	for(size_t i=0; i<ret_len; i++) {
		assert(t[i] == ret[i]);
	}

out:
	if(t)
		free(t);
	if(s)
		free(s);
	if(ret)
		free(ret);
	ecc_point_clear(&pkr);
	ecc_point_clear(&out);
	ecc_scalar_clear(&skr);
	suite_clear(&s_ctx);

	return r;
}
