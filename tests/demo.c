// The receiver part of the client-server communication model

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/random.h>
#include <pthread.h>

#include "../hkdf.h"
#include "../hpke_types.h"
#include "../hpke.h"

#define PIPE_READ  0
#define PIPE_WRITE 1

#define INFO		"CHAT DEMO"
#define INFO_LEN	9

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

void *
receiver(void *args) {
	int pipe_fd[2] = {((int*)args)[0], ((int*)args)[1]};
	uint8_t *skR;
	uint8_t *pkR;
	uint8_t *enc;
	char *enc_ser;
	struct suite_ctx s_ctx;
	struct context ctx;
	uint8_t *ct;
	size_t ct_len;
	uint8_t *pt;
	size_t pt_len;

	suite_init(&s_ctx, HPKE_DHKEM_X25519, HPKE_HKDF_SHA256, HPKE_AEAD_CHACHA20POLY1305);

	uint8_t entropy[s_ctx.nsk];

	if(getrandom((void*)entropy, s_ctx.nsk, 0) < s_ctx.nsk)
		pthread_exit(NULL);

	derive_key_pair_x(entropy, s_ctx.nsk, &s_ctx, &skR, &pkR);

	char *tmp0;
	char *tmp1;

	octet_to_hex_string(&tmp0, skR, s_ctx.nsk);
	octet_to_hex_string(&tmp1, pkR, s_ctx.nsk);

	printf("%s> %s: %s\n", "Receiver", "public key", tmp1);
	printf("%s> %s: %s\n", "Receiver", "private key", tmp0);

	free(tmp0);
	free(tmp1);

	printf("%s> %s\n", "Receiver", "Sending public key..");
	pthread_cond_signal(&cond);
	write(pipe_fd[PIPE_WRITE], &(s_ctx.nsk), 1);
	write(pipe_fd[PIPE_WRITE], (void*)pkR, s_ctx.nsk);
	enc = calloc(s_ctx.nsk, 1);

	pthread_mutex_lock(&mutex);
	pthread_cond_wait(&cond, &mutex);
	pthread_mutex_unlock(&mutex);

	read(pipe_fd[PIPE_READ], (void*)enc, s_ctx.nsk);

	octet_to_hex_string(&enc_ser, enc, s_ctx.nsk);
	printf("%s> %s: %s\n", "Receiver", "Got public key", enc_ser);

	setup_base_r(enc, s_ctx.nsk, pkR, s_ctx.nsk, skR, s_ctx.nsk, (uint8_t*)INFO, INFO_LEN, HPKE_DHKEM_X25519, HPKE_HKDF_SHA256, HPKE_AEAD_CHACHA20POLY1305, &ctx);
	printf("%s> %s\n", "Receiver", "Encryption context set up");

	free(skR);
	free(pkR);
	free(enc);
	free(enc_ser);
	suite_clear(&s_ctx);
	
	while(1) {
		pthread_cond_signal(&cond);

		printf("%s> %s\n", "Receiver", "Waiting for a message..");
		read(pipe_fd[PIPE_READ], (void*)&ct_len, sizeof(size_t));
		ct = calloc(ct_len, 1);
		read(pipe_fd[PIPE_READ], (void*)ct, ct_len);
		if (!memcmp(ct, "/quit", 5))
			break;
		char *tmp;
		octet_to_hex_string(&tmp, ct, ct_len);
		printf("%s> %s: %s\n", "Receiver", "Got ciphertext", tmp);
		free(tmp);

		hpke_open(&ctx, NULL, 0, ct, ct_len, &pt, &pt_len);
		printf("%s> %s: ", "Receiver", "The decripted message");
		for(size_t i=0; i<pt_len; i++)
			printf("%c", pt[i]);
		printf("\n");

		free(ct);
		free(pt);
	}

	free(ct);
	context_clear(&ctx);

	return NULL;
}

void *
sender(void *args) {
	int pipe_fd[2] = {((int*)args)[0], ((int*)args)[1]};
	uint8_t pkR_len;
	uint8_t *pkR;
	char *pkR_ser;
	struct context ctx;
	uint8_t *input = calloc(100, 1);
	uint8_t *ct;
	size_t ct_len;

	pthread_mutex_lock(&mutex);
	pthread_cond_wait(&cond, &mutex);
	pthread_mutex_unlock(&mutex);

	read(pipe_fd[PIPE_READ], &pkR_len, 1);
	pkR = calloc(pkR_len, 1);
	read(pipe_fd[PIPE_READ], (void*)pkR, pkR_len);
	
	octet_to_hex_string(&pkR_ser, pkR, pkR_len);
	printf("%s> %s: %s\n", "Sender", "Got public key", pkR_ser);
	
	setup_base_s(pkR, pkR_len, (uint8_t*)INFO, INFO_LEN, HPKE_DHKEM_X25519, HPKE_HKDF_SHA256, HPKE_AEAD_CHACHA20POLY1305, &ctx);
	printf("%s> %s\n", "Sender", "Encryption context set up");

	free(pkR);
	free(pkR_ser);

	printf("%s> %s\n", "Sender", "Sending public key..");
	pthread_cond_signal(&cond);
	write(pipe_fd[PIPE_WRITE], (void*)(ctx.s_ctx.enc), ctx.s_ctx.nsk);

	while(1) {
		pthread_mutex_lock(&mutex);
		pthread_cond_wait(&cond, &mutex);
		pthread_mutex_unlock(&mutex);

		printf("%s> Enter plaintext: ", "Sender");
		fgets((char*)input, 100, stdin);
		input[strlen((char*)input)-1] = '\0';
		if (!memcmp(input, "/quit", 5)) {
                        ct_len = 5;
			write(pipe_fd[PIPE_WRITE], (void*)&ct_len, sizeof(size_t));
			write(pipe_fd[PIPE_WRITE], "/quit", 5);
			break;
		}

		hpke_seal(&ctx, NULL, 0, input, strlen((char*)input), &ct, &ct_len);
		char *tmp;
		octet_to_hex_string(&tmp, ct, ct_len);
		printf("%s> %s: %s\n", "Sender", "Sending encrypted message", tmp);
		free(tmp);

		write(pipe_fd[PIPE_WRITE], (void*)&ct_len, sizeof(size_t));
		write(pipe_fd[PIPE_WRITE], (void*)ct, ct_len);

		free(ct);
	}

	free(input);
	context_clear(&ctx);

	return NULL;
}

int
main (void) {
	int pipe_fd[2];
	
	if (pipe(pipe_fd) < 0)
		return -1;

	pthread_t send_pt;
	pthread_t recv_pt;

	pthread_create(&recv_pt, NULL, receiver, (void*)&pipe_fd);
	pthread_create(&send_pt, NULL, sender, (void*)&pipe_fd);

	pthread_join(recv_pt, NULL);
	pthread_join(send_pt, NULL);

	pthread_cond_destroy(&cond);
	pthread_mutex_destroy(&mutex);

	close(pipe_fd[0]);
	close(pipe_fd[1]);

	return 0;
}
