/* FIXME Insert license here */

#include "hkdf.h"

int
derive_key_pair_p (uint8_t *ikm, size_t ikm_len, struct suite_ctx *s_ctx,
                   struct ecc_scalar *sk, struct ecc_point *pk)
{
  int r;
  uint8_t *dkp_prk, *bytes;
  mpz_t z, counter, order;

  mpz_init(z);
  mpz_init_set_ui(counter, 0);
  mpz_init_set_str(order, s_ctx->ecc_order, 10);

  dkp_prk = labeled_extract_kem(NULL, 0, "dkp_prk", ikm, ikm_len, s_ctx);
  if (!dkp_prk)
    {
      r = -1;
      goto out;
    }
  while(mpz_cmp_ui(z, 0) == 0 || mpz_cmp(z, order) != -1)
    {
      if (mpz_cmp_ui(counter, 255) == 1)
        {
          r = -1;
          goto out;
        }
      uint8_t counter_oct[1];
      nettle_mpz_get_str_256(1, counter_oct, z);
      bytes = labeled_expand_kem(dkp_prk,
                                 s_ctx->digest_size,
                                 "candidate",
                                 counter_oct,
                                 1,
                                 s_ctx->nsk,
                                 s_ctx);
      if (!bytes)
        {
          r = -1;
          goto out;
        }
      if (s_ctx->dhkem == HPKE_DHKEM_P256)
        bytes[0] &= 0xff;
      else if (s_ctx->dhkem == HPKE_DHKEM_P384)
        bytes[0] &= 0xff;
      else
        bytes[0] &= 0x01;

      nettle_mpz_set_str_256_u(z, s_ctx->nsk, bytes);
      mpz_add_ui(counter, counter, 1);
      free(bytes);
    }

  ecc_scalar_set(sk, z);

  ecc_point_mul_g(pk, sk);

out:
  if (dkp_prk)
    free(dkp_prk);
  mpz_clear(z);
  mpz_clear(counter);
  mpz_clear(order);

  return r;
}

int
derive_key_pair_x (uint8_t *ikm, size_t ikm_len, struct suite_ctx *s_ctx,
                   uint8_t **sk, uint8_t **pk)
{
  uint8_t *dkp_prk;
  *pk = calloc(s_ctx->nsk, 1);
  if (!*pk)
    return -1;

  dkp_prk = labeled_extract_kem(NULL, 0, "dkp_prk", ikm, ikm_len, s_ctx);
  if (!dkp_prk)
    {
      free(pk);
      return -1;
    }
  *sk = labeled_expand_kem(dkp_prk,
                           s_ctx->digest_size,
                           "sk",
                           NULL,
                           0,
                           s_ctx->nsk,
                           s_ctx);
  if (!*sk)
    {
      free(pk);
      free(dkp_prk);
      return -1;
    }

  if (s_ctx->dhkem == HPKE_DHKEM_X25519)
    curve25519_mul_g(*pk, *sk);
  else if (s_ctx->dhkem == HPKE_DHKEM_X448)
    curve448_mul_g(*pk, *sk);

  free(dkp_prk);

  return 0;
}
