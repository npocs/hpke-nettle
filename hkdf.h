/* FIXME Insert license here */

#ifndef __HPKE_NETTLE_HKDF_H__
#define __HPKE_NETTLE_HKDF_H__

#include "hpke_types.h"

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <nettle/hkdf.h>
#include <nettle/sha2.h>
#include <nettle/curve25519.h>
#include <nettle/curve448.h>
#include <nettle/hmac.h>

/* the sizes of the outputs of extract and expand functions are
 * dependent on the digest size */
uint8_t *
labeled_extract_kem (uint8_t *salt, size_t salt_len, char *label,
                     uint8_t *ikm, size_t ikm_len, struct suite_ctx *s_ctx);

uint8_t *
labeled_expand_kem (uint8_t *prk, size_t prk_len, char *label,
                    uint8_t *info, size_t info_len, uint16_t l,
                    struct suite_ctx *s_ctx);

uint8_t *
extract_and_expand_kem (uint8_t *dh, size_t dh_len,
                        uint8_t *kem_context, size_t kem_context_len,
                        struct suite_ctx *s_ctx);

uint8_t *
labeled_extract_hpke (uint8_t *salt, size_t salt_len, char *label,
                      uint8_t *ikm, size_t ikm_len, struct suite_ctx *s_ctx);

uint8_t *
labeled_expand_hpke (uint8_t *prk, size_t prk_len, char *label,
                     uint8_t *info, size_t info_len, uint16_t l,
                     struct suite_ctx *s_ctx);

int
derive_key_pair_p (uint8_t *ikm, size_t ikm_len, struct suite_ctx *s_ctx,
                   struct ecc_scalar *sk, struct ecc_point *pk);

int
derive_key_pair_x (uint8_t *ikm, size_t ikm_len, struct suite_ctx *s_ctx,
                   uint8_t **sk, uint8_t **pk);

#endif /* __HPKE_NETTLE_HKDF_H__ */
