/* TODO Insert license here */

#include "dhkem.h"

/* pkR should be a hex string of an octet string representing the
 * point on the ec */
int
encap (uint8_t *pkr, size_t pkr_len, struct suite_ctx *s_ctx)
{
  uint8_t *enc, *dh_ser, *kem_context;
  size_t dh_ser_len, enc_len;
  uint8_t entropy[s_ctx->nsk];

  if (getrandom((void*)entropy, s_ctx->nsk, 0) < s_ctx->nsk)
    return -1;

  if (s_ctx->dhkem == HPKE_DHKEM_P256
      || s_ctx->dhkem == HPKE_DHKEM_P384
      || s_ctx->dhkem == HPKE_DHKEM_P521)
    {
      struct ecc_point p, dh, pR;
      struct ecc_scalar key;
      mpz_t x;

      ecc_point_init(&pR, s_ctx->ecc);
      octet_to_ecc_point(&pR, pkr, pkr_len, s_ctx);

      ecc_point_init(&p, s_ctx->ecc);
      ecc_scalar_init(&key, s_ctx->ecc);

      derive_key_pair_p(entropy, s_ctx->nsk, s_ctx, &key, &p);

      ecc_point_init(&dh, s_ctx->ecc);
      ecc_point_mul(&dh, &key, &pR);

      mpz_init(x);
      ecc_point_get(&dh, x, NULL);
      dh_ser_len = (ecc_bit_size(s_ctx->ecc) + 7) / 8;
      dh_ser = calloc(dh_ser_len, 1);
      if (!dh_ser)
        {
          mpz_clear(x);
          ecc_point_clear(&p);
          ecc_point_clear(&pR);
          ecc_point_clear(&dh);
          ecc_scalar_clear(&key);
          return -1;
        }
      nettle_mpz_get_str_256(dh_ser_len, dh_ser, x);
      mpz_clear(x);

      ecc_point_to_octet(&enc, &enc_len, &p, s_ctx);

      ecc_point_clear(&p);
      ecc_point_clear(&pR);
      ecc_point_clear(&dh);
      ecc_scalar_clear(&key);
    }
  else
    {
      uint8_t *sk;

      if (derive_key_pair_x(entropy, s_ctx->nsk, s_ctx, &sk, &enc))
        return -1;
      enc_len = s_ctx->nsk;

      dh_ser = calloc(s_ctx->nsk, 1);
      if (!dh_ser)
        {
          free(sk);
          free(enc);
          return -1;
        }
      dh_ser_len = s_ctx->nsk;

      if (s_ctx->dhkem == HPKE_DHKEM_X25519)
          curve25519_mul(dh_ser, sk, pkr);
      else if (s_ctx->dhkem == HPKE_DHKEM_X448)
          curve448_mul(dh_ser, sk, pkr);

      free(sk);
    }

  kem_context = calloc(enc_len + pkr_len, 1);
  if (!kem_context)
    {
      free(enc);
      free(dh_ser);
      return -1;
    }
  memcpy(kem_context, enc, enc_len);
  memcpy(kem_context + enc_len, pkr, pkr_len);

  s_ctx->shared_secret = extract_and_expand_kem(dh_ser,
                                                dh_ser_len,
                                                kem_context,
                                                enc_len+pkr_len,
                                                s_ctx);
  if (!s_ctx->shared_secret)
    {
      free(enc);
      free(dh_ser);
      free(kem_context);
      return -1;
    }
  s_ctx->enc = enc;
  s_ctx->enc_len = enc_len;

  free(dh_ser);
  free(kem_context);

  return 0;
}

int
decap (uint8_t *enc, size_t enc_len, uint8_t *pkr, size_t pkr_len, uint8_t *skr,
       size_t skr_len, struct suite_ctx *s_ctx)
{
  uint8_t *kem_context, *dh_ser;
  size_t dh_ser_len;

  if (s_ctx->dhkem == HPKE_DHKEM_P256
      || s_ctx->dhkem == HPKE_DHKEM_P384
      || s_ctx->dhkem == HPKE_DHKEM_P521)
    {
      struct ecc_point pkE, dh;
      struct ecc_scalar skR;
      char *skrm;
      mpz_t x, z;
      ecc_point_init(&pkE, s_ctx->ecc);
      ecc_point_init(&dh, s_ctx->ecc);
      ecc_scalar_init(&skR, s_ctx->ecc);

      octet_to_hex_string(&skrm, skr, skr_len);
      mpz_init_set_str(z, skrm, 16);
      ecc_scalar_set(&skR, z);

      octet_to_ecc_point(&pkE, enc, enc_len, s_ctx);

      ecc_point_mul(&dh, &skR, &pkE);

      mpz_init(x);
      ecc_point_get(&dh, x, NULL);
      dh_ser_len = (ecc_bit_size(s_ctx->ecc) + 7) / 8;
      dh_ser = calloc(dh_ser_len, 1);
      if (!dh_ser)
        {
          mpz_clear(x);
          ecc_point_clear(&pkE);
          ecc_point_clear(&dh);
          ecc_scalar_clear(&skR);
          free(skrm);
          mpz_clear(z);
          return -1;
        }
      nettle_mpz_get_str_256(dh_ser_len, dh_ser, x);
      mpz_clear(x);

      ecc_point_clear(&pkE);
      ecc_point_clear(&dh);
      ecc_scalar_clear(&skR);
      free(skrm);
      mpz_clear(z);
    }
  else if (s_ctx->dhkem == HPKE_DHKEM_X25519)
    {
      dh_ser = calloc(s_ctx->nsk, 1);
      if (!dh_ser)
        return -1;
      dh_ser_len = s_ctx->nsk;
      curve25519_mul(dh_ser, skr, enc);
    }
  else if (s_ctx->dhkem == HPKE_DHKEM_X448)
    {
      dh_ser = calloc(s_ctx->nsk, 1);
      if (!dh_ser)
        return -1;
      dh_ser_len = s_ctx->nsk;
      curve448_mul(dh_ser, skr, enc);
    }

  kem_context = calloc(enc_len + pkr_len, 1);
  if (!kem_context)
    {
      free(dh_ser);
      return -1;
    }
  memcpy(kem_context, enc, enc_len);
  memcpy(kem_context + enc_len, pkr, pkr_len);

  int r = 0;
  s_ctx->shared_secret = extract_and_expand_kem(dh_ser,
                                                dh_ser_len,
                                                kem_context,
                                                enc_len+pkr_len,
                                                s_ctx);
  if (!s_ctx->shared_secret)
    r = -1;

  free(dh_ser);
  free(kem_context);

  return r;
}

#ifdef TEST
int
encap_testable (char *pkR, char *pkE, char *skE, struct suite_ctx *s_ctx)
{
  uint8_t *enc, *pkRm, *dh_ser, *kem_context;
  size_t dh_ser_len, enc_len, pkRm_len;

  hex_string_to_octet(&pkRm, &pkRm_len, pkR);

  if (s_ctx->dhkem == HPKE_DHKEM_P256
      || s_ctx->dhkem == HPKE_DHKEM_P384
      || s_ctx->dhkem == HPKE_DHKEM_P521)
    {
      uint8_t *pke_o, *ske_o;
      size_t pke_o_len, ske_o_len;
      struct ecc_point p, dh, pR;
      struct ecc_scalar key;
      mpz_t x, z;

      ecc_point_init(&pR, s_ctx->ecc);
      octet_to_ecc_point(&pR, pkRm, pkRm_len, s_ctx);

      ecc_point_init(&p, s_ctx->ecc);
      ecc_scalar_init(&key, s_ctx->ecc);

      mpz_init(z);

      hex_string_to_octet(&pke_o, &pke_o_len, pkE);
      hex_string_to_octet(&ske_o, &ske_o_len, skE);
      octet_to_ecc_point(&p, pke_o, pke_o_len, s_ctx);
      nettle_mpz_set_str_256_u(z, ske_o_len, ske_o);
      ecc_scalar_set(&key, z);

      mpz_clear(z);

      ecc_point_init(&dh, s_ctx->ecc);
      ecc_point_mul(&dh, &key, &pR);

      mpz_init(x);
      ecc_point_get(&dh, x, NULL);
      dh_ser_len = (ecc_bit_size(s_ctx->ecc) + 7) / 8;
      dh_ser = calloc(dh_ser_len, 1);
      nettle_mpz_get_str_256(dh_ser_len, dh_ser, x);
      mpz_clear(x);

      ecc_point_to_octet(&enc, &enc_len, &p, s_ctx);

      free(pke_o);
      free(ske_o);
      ecc_point_clear(&p);
      ecc_point_clear(&pR);
      ecc_point_clear(&dh);
      ecc_scalar_clear(&key);
    }
  else
    {
      uint8_t *sk;
      size_t sk_len;

      hex_string_to_octet(&enc, &enc_len, pkE);
      hex_string_to_octet(&sk, &sk_len, skE);

      if (s_ctx->dhkem == HPKE_DHKEM_X25519)
        {
          dh_ser = calloc(s_ctx->nsk, 1);
          dh_ser_len = s_ctx->nsk;
          curve25519_mul(dh_ser, sk, pkRm);
        }
      else if (s_ctx->dhkem == HPKE_DHKEM_X448)
        {
          dh_ser = calloc(s_ctx->nsk, 1);
          dh_ser_len = s_ctx->nsk;
          curve448_mul(dh_ser, sk, pkRm);
        }
      free(sk);
    }

  kem_context = calloc(enc_len + pkRm_len, 1);
  memcpy(kem_context, enc, enc_len);
  memcpy(kem_context + enc_len, pkRm, pkRm_len);

  s_ctx->shared_secret = extract_and_expand_kem(dh_ser, dh_ser_len, kem_context, enc_len+pkRm_len, s_ctx);
  s_ctx->enc = enc;
  s_ctx->enc_len = enc_len;

  free(dh_ser);
  free(pkRm);
  free(kem_context);

  return 0;
}
#endif /* TEST */
