/* TODO Insert license here */

#include "hpke.h"

int
key_schedule (struct enc_ctx *e_ctx, struct suite_ctx *s_ctx,
    struct context *c, const uint8_t mode)
{
  uint8_t *psk_id_hash = NULL;
  uint8_t *info_hash = NULL;
  uint8_t *key_schedule_cnt = NULL;
  uint8_t *secret = NULL;
  uint8_t *key = NULL;
  uint8_t *base_nonce = NULL;
  uint8_t *exporter_secret = NULL;
  int r = 0;

  const bool got_psk = (e_ctx->psk && e_ctx->psk_len);
  const bool got_psk_id = (e_ctx->psk_id && e_ctx->psk_id_len);

  if(got_psk != got_psk_id)
    //TODO raise Incosistent PSK inputs
    return -1;

  if(got_psk
     && (e_ctx->mode == HPKE_MODE_BASE || e_ctx->mode == HPKE_MODE_AUTH))
    //TODO raise PSK input provided when not needed
    return -1;

  if(!got_psk
     && (e_ctx->mode == HPKE_MODE_PSK || e_ctx->mode == HPKE_MODE_AUTH_PSK))
    //TODO raise Missing required PSK input
    return -1;

  psk_id_hash = labeled_extract_hpke(NULL,
                                     0,
                                     "psk_id_hash",
                                     e_ctx->psk_id,
                                     e_ctx->psk_id_len,
                                     s_ctx);
  if (!psk_id_hash)
    {
      r = -1;
      goto out;
    }
  info_hash = labeled_extract_hpke(NULL,
                                   0,
                                   "info_hash",
                                   e_ctx->info,
                                   e_ctx->info_len,
                                   s_ctx);
  if (!info_hash)
    {
      r = -1;
      goto out;
    }

  const size_t key_schedule_cnt_len = 2*(s_ctx->digest_size)+1;

  key_schedule_cnt = calloc(key_schedule_cnt_len, 1);
  if (!key_schedule_cnt)
    {
      r = -1;
      goto out;
    }
  *key_schedule_cnt = e_ctx->mode;
  memcpy(key_schedule_cnt+1, psk_id_hash, s_ctx->digest_size);
  memcpy(key_schedule_cnt+1+s_ctx->digest_size, info_hash, s_ctx->digest_size);

  secret = labeled_extract_hpke(s_ctx->shared_secret,
                                s_ctx->Nsecret,
                                "secret",
                                e_ctx->psk,
                                e_ctx->psk_len,
                                s_ctx);
  if (!secret)
    {
      r = -1;
      goto out;
    }

  if (s_ctx->aead != HPKE_AEAD_EXPORT_ONLY)
    {
      key = labeled_expand_hpke(secret,
                                s_ctx->digest_size,
                                "key",
                                key_schedule_cnt,
                                key_schedule_cnt_len,
                                s_ctx->nk,
                                s_ctx);
      if (!key)
        {
          r = -1;
          goto out;
        }

      base_nonce = labeled_expand_hpke(secret,
                                       s_ctx->digest_size,
                                       "base_nonce",
                                       key_schedule_cnt,
                                       key_schedule_cnt_len,
                                       HPKE_AEAD_NN,
                                       s_ctx);
      if (!base_nonce)
        {
          r = -1;
          goto out;
        }
    }

  exporter_secret = labeled_expand_hpke(secret,
                                        s_ctx->digest_size,
                                        "exp",
                                        key_schedule_cnt,
                                        key_schedule_cnt_len,
                                        s_ctx->nh,
                                        s_ctx);
  if (!exporter_secret)
    {
      r = -1;
      goto out;
    }

  if (context_init(c, key, base_nonce, exporter_secret, s_ctx))
    r = -1;

out:
  if(psk_id_hash)
    free(psk_id_hash);
  if(info_hash)
    free(info_hash);
  if(key_schedule_cnt)
    free(key_schedule_cnt);
  if(secret)
    free(secret);
  if(key)
    free(key);
  if(base_nonce)
    free(base_nonce);
  if(exporter_secret)
    free(exporter_secret);

  return r;
}

int
setup_base_s (uint8_t *pkr, size_t pkr_len, uint8_t *info, size_t info_len,
              const uint16_t dhkem, const uint16_t hkdf, const uint16_t aead,
              struct context *c)
{
  struct suite_ctx s;
  struct enc_ctx e;
  int r;

  suite_init(&s, dhkem, hkdf, aead);

  r = encap(pkr, pkr_len, &s);
  if (r < 0)
    goto out;

  r = enc_init(&e, HPKE_MODE_BASE, info, info_len, NULL, 0, NULL, 0);
  if (r < 0)
    goto out;

  r = key_schedule(&e, &s, c, HPKE_MODE_BASE);
  if (r < 0)
    goto out;

out:
  enc_clear(&e);

  return r;
}

int
setup_base_r (uint8_t *enc, size_t enc_len, uint8_t *pkr, size_t pkr_len,
              uint8_t *skr, size_t skr_len, uint8_t *info, size_t info_len,
              const uint16_t dhkem, const uint16_t hkdf, const uint16_t aead,
              struct context *c)
{
  int r = 0;
  struct suite_ctx s;
  struct enc_ctx e;

  suite_init(&s, dhkem, hkdf, aead);

  r = decap(enc, enc_len, pkr, pkr_len, skr, skr_len, &s);
  if (r < 0)
    goto out;

  r = enc_init(&e, HPKE_MODE_BASE, info, info_len, NULL, 0, NULL, 0);
  if (r < 0)
    goto out;

  r = key_schedule(&e, &s, c, HPKE_MODE_BASE);

out:
  enc_clear(&e);

  return r;
}

int
setup_psk_s (uint8_t *pkr, size_t pkr_len, uint8_t *info, size_t info_len,
             uint8_t *psk, size_t psk_len, uint8_t *psk_id, size_t psk_id_len,
             const uint16_t dhkem, const uint16_t hkdf, const uint16_t aead,
             struct context *c)
{
  struct suite_ctx s;
  struct enc_ctx e;
  int r;

  suite_init(&s, dhkem, hkdf, aead);

  r = encap(pkr, pkr_len, &s);
  if (r < 0)
    goto out;

  r = enc_init(&e, HPKE_MODE_PSK, info, info_len, psk, psk_len,
               psk_id, psk_id_len);
  if (r < 0)
    goto out;

  r = key_schedule(&e, &s, c, HPKE_MODE_PSK);
  if (r < 0)
    goto out;

out:
  enc_clear(&e);

  return r;
}

int
setup_psk_r (uint8_t *enc, size_t enc_len, uint8_t *pkr, size_t pkr_len,
             uint8_t *skr, size_t skr_len, uint8_t *info, size_t info_len,
             uint8_t *psk, size_t psk_len, uint8_t *psk_id, size_t psk_id_len,
             const uint16_t dhkem, const uint16_t hkdf, const uint16_t aead,
             struct context *c)
{
  int r = 0;
  struct suite_ctx s;
  struct enc_ctx e;

  suite_init(&s, dhkem, hkdf, aead);

  r = decap(enc, enc_len, pkr, pkr_len, skr, skr_len, &s);
  if (r < 0)
    goto out;

  r = enc_init(&e, HPKE_MODE_PSK, info, info_len, psk, psk_len,
               psk_id, psk_id_len);
  if (r < 0)
    goto out;

  r = key_schedule(&e, &s, c, HPKE_MODE_PSK);

out:
  enc_clear(&e);

  return r;
}

int
setup_auth_s (uint8_t *pkr, size_t pkr_len,
              uint8_t *pks, size_t pks_len,
              uint8_t *sks, size_t sks_len,
              uint8_t *info, size_t info_len,
              const uint16_t dhkem, const uint16_t hkdf, const uint16_t aead,
              struct context *c)
{
  struct suite_ctx s;
  struct enc_ctx e;
  int r;

  suite_init(&s, dhkem, hkdf, aead);

  r = encap_auth(pkr, pkr_len, pks, pks_len, sks, sks_len, &s);
  if (r < 0)
    goto out;

  r = enc_init(&e, HPKE_MODE_AUTH, info, info_len, NULL, 0, NULL, 0);
  if (r < 0)
    goto out;

  r = key_schedule(&e, &s, c, HPKE_MODE_AUTH);
  if (r < 0)
    goto out;

out:
  enc_clear(&e);

  return r;
}

int
setup_auth_r (uint8_t *enc, size_t enc_len, uint8_t *pks, size_t pks_len,
              uint8_t *pkr, size_t pkr_len, uint8_t *skr, size_t skr_len,
              uint8_t *info, size_t info_len,
              const uint16_t dhkem, const uint16_t hkdf, const uint16_t aead,
              struct context *c)
{
  int r = 0;
  struct suite_ctx s;
  struct enc_ctx e;

  suite_init(&s, dhkem, hkdf, aead);

  r = decap_auth(enc, enc_len, pks, pks_len, pkr, pkr_len, skr, skr_len, &s);
  if (r < 0)
    goto out;

  r = enc_init(&e, HPKE_MODE_AUTH, info, info_len, NULL, 0, NULL, 0);
  if (r < 0)
    goto out;

  r = key_schedule(&e, &s, c, HPKE_MODE_AUTH);

out:
  enc_clear(&e);

  return r;
}

int
setup_auth_psk_s (uint8_t *pkr, size_t pkr_len,
                  uint8_t *pks, size_t pks_len,
                  uint8_t *sks, size_t sks_len,
                  uint8_t *psk, size_t psk_len,
                  uint8_t *psk_id, size_t psk_id_len,
                  uint8_t *info, size_t info_len,
                  const uint16_t dhkem, const uint16_t hkdf, const uint16_t aead,
                  struct context *c)
{
  struct suite_ctx s;
  struct enc_ctx e;
  int r;

  suite_init(&s, dhkem, hkdf, aead);

  r = encap_auth(pkr, pkr_len, pks, pks_len, sks, sks_len, &s);
  if (r < 0)
    goto out;

  r = enc_init(&e, HPKE_MODE_AUTH_PSK, info, info_len, psk, psk_len,
               psk_id, psk_id_len);
  if (r < 0)
    goto out;

  r = key_schedule(&e, &s, c, HPKE_MODE_AUTH_PSK);
  if (r < 0)
    goto out;

out:
  enc_clear(&e);

  return r;
}

int
setup_auth_psk_r (uint8_t *enc, size_t enc_len, uint8_t *pks, size_t pks_len,
                  uint8_t *pkr, size_t pkr_len, uint8_t *skr, size_t skr_len,
                  uint8_t *psk, size_t psk_len, uint8_t *psk_id,
                  size_t psk_id_len, uint8_t *info, size_t info_len,
                  const uint16_t dhkem, const uint16_t hkdf,
                  const uint16_t aead, struct context *c)
{
  int r = 0;
  struct suite_ctx s;
  struct enc_ctx e;

  suite_init(&s, dhkem, hkdf, aead);

  r = decap_auth(enc, enc_len, pks, pks_len, pkr, pkr_len, skr, skr_len, &s);
  if (r < 0)
    goto out;

  r = enc_init(&e, HPKE_MODE_AUTH_PSK, info, info_len, psk, psk_len,
               psk_id, psk_id_len);
  if (r < 0)
    goto out;

  r = key_schedule(&e, &s, c, HPKE_MODE_AUTH_PSK);

out:
  enc_clear(&e);

  return r;
}

#ifdef TEST
int
setup_base_s_testable (char *pkR, char *pkE, char *skE, uint8_t *info,
                       size_t info_len, const uint16_t dhkem,
                       const uint16_t hkdf, const uint16_t aead,
                       struct context *c)
{
  struct suite_ctx s;
  struct enc_ctx e;
  int r = 0;

  suite_init(&s, dhkem, hkdf, aead);

  r = encap_testable(pkR, pkE, skE, &s);
  if (r < 0)
    goto out;

  r = enc_init(&e, HPKE_MODE_BASE, info, info_len, NULL, 0, NULL, 0);
  if (r < 0)
    goto out;

  r = key_schedule(&e, &s, c, HPKE_MODE_BASE);
  if (r < 0)
    goto out;

out:
  enc_clear(&e);

  return r;
}

int
setup_psk_s_testable (char *pkR, char *pkE, char *skE,
                      uint8_t *info, size_t info_len,
                      uint8_t *psk, size_t psk_len, uint8_t *psk_id,
                      size_t psk_id_len, const uint16_t dhkem,
                      const uint16_t hkdf, const uint16_t aead,
                      struct context *c)
{
  struct suite_ctx s;
  struct enc_ctx e;
  int r = 0;

  suite_init(&s, dhkem, hkdf, aead);

  r = encap_testable(pkR, pkE, skE, &s);
  if (r < 0)
    goto out;

  r = enc_init(&e, HPKE_MODE_PSK, info, info_len, psk, psk_len,
               psk_id, psk_id_len);
  if (r < 0)
    goto out;

  r = key_schedule(&e, &s, c, HPKE_MODE_PSK);
  if (r < 0)
    goto out;

out:
  enc_clear(&e);

  return r;
}

int
setup_auth_s_testable (char *pkr, char *pks, char *sks,
                       char *pke, char *ske,
                       uint8_t *info, size_t info_len,
                       const uint16_t dhkem, const uint16_t hkdf,
                       const uint16_t aead, struct context *c)
{
  struct suite_ctx s;
  struct enc_ctx e;
  int r;

  suite_init(&s, dhkem, hkdf, aead);

  r = encap_auth_testable(pkr, pke, ske, pks, sks, &s);
  if (r < 0)
    goto out;

  r = enc_init(&e, HPKE_MODE_AUTH, info, info_len, NULL, 0, NULL, 0);
  if (r < 0)
    goto out;

  r = key_schedule(&e, &s, c, HPKE_MODE_AUTH);
  if (r < 0)
    goto out;

out:
  enc_clear(&e);

  return r;
}

int
setup_auth_psk_s_testable (char *pkr, char *pks, char *sks,
                           char *pke, char *ske,
                           uint8_t *psk, size_t psk_len,
                           uint8_t *psk_id, size_t psk_id_len,
                           uint8_t *info, size_t info_len,
                           const uint16_t dhkem, const uint16_t hkdf,
                           const uint16_t aead, struct context *c)
{
  struct suite_ctx s;
  struct enc_ctx e;
  int r;

  suite_init(&s, dhkem, hkdf, aead);

  r = encap_auth_testable(pkr, pke, ske, pks, sks, &s);
  if (r < 0)
    goto out;

  r = enc_init(&e, HPKE_MODE_AUTH_PSK, info, info_len, psk, psk_len,
               psk_id, psk_id_len);
  if (r < 0)
    goto out;

  r = key_schedule(&e, &s, c, HPKE_MODE_AUTH_PSK);
  if (r < 0)
    goto out;

out:
  enc_clear(&e);

  return r;
}
#endif /* TEST */

int
hpke_seal (struct context *ctx, uint8_t *aad, size_t aad_len, uint8_t *pt,
      size_t pt_len, uint8_t **ct, size_t *ct_len)
{
  if (ctx->s_ctx.aead == HPKE_AEAD_EXPORT_ONLY)
    return -1;
  int r;
  uint8_t *seq_octet = calloc(HPKE_AEAD_NN, 1);
  uint8_t *nonce = calloc(HPKE_AEAD_NN, 1);

  if (!seq_octet || !nonce)
    {
      r = -1;
      goto out;
    }
  r = i2osp(ctx->seq_n, seq_octet, HPKE_AEAD_NN);
  if (r)
    goto out;

  xor_octet_arr(nonce, ctx->base_nonce, seq_octet, HPKE_AEAD_NN);

  if (ctx->s_ctx.aead == HPKE_AEAD_AES_128_GCM)
    {
      *ct_len = pt_len + GCM_DIGEST_SIZE;
      *ct = calloc(*ct_len, 1);
      if (!*ct)
        {
          r = -1;
          goto out;
        }
      struct gcm_aes128_ctx c;
      gcm_aes128_set_key(&c, ctx->key);
      gcm_aes128_set_iv(&c, HPKE_AEAD_NN, nonce);
      gcm_aes128_update(&c, aad_len, aad);
      gcm_aes128_encrypt(&c, pt_len, *ct, pt);
      gcm_aes128_digest(&c, GCM_DIGEST_SIZE, (*ct)+pt_len);
    }
  else if (ctx->s_ctx.aead == HPKE_AEAD_AES_256_GCM)
    {
      *ct_len = pt_len + GCM_DIGEST_SIZE;
      *ct = calloc(*ct_len, 1);
      if (!*ct)
        {
          r = -1;
          goto out;
        }
      struct gcm_aes256_ctx c;
      gcm_aes256_set_key(&c, ctx->key);
      gcm_aes256_set_iv(&c, HPKE_AEAD_NN, nonce);
      gcm_aes256_update(&c, aad_len, aad);
      gcm_aes256_encrypt(&c, pt_len, *ct, pt);
      gcm_aes256_digest(&c, GCM_DIGEST_SIZE, (*ct)+pt_len);
    }
  else if (ctx->s_ctx.aead == HPKE_AEAD_CHACHA20POLY1305)
  {
    *ct_len = pt_len + CHACHA_POLY1305_DIGEST_SIZE;
    *ct = calloc(*ct_len, 1);
    if (!*ct)
      {
        r = -1;
        goto out;
      }
    struct chacha_poly1305_ctx c;
    chacha_poly1305_set_key(&c, ctx->key);
    chacha_poly1305_set_nonce(&c, nonce);
    chacha_poly1305_update(&c, aad_len, aad);
    chacha_poly1305_encrypt(&c, pt_len, *ct, pt);
    chacha_poly1305_digest(&c, CHACHA_POLY1305_DIGEST_SIZE, (*ct)+pt_len);
  }

  ++ctx->seq_n;

out:
  free(seq_octet);
  free(nonce);

  return r;
}

int
hpke_open (struct context *ctx, uint8_t *aad, size_t aad_len, uint8_t *ct,
      size_t ct_len, uint8_t **pt, size_t *pt_len)
{
  if (ctx->s_ctx.aead == HPKE_AEAD_EXPORT_ONLY)
    return -1;
  int r = 0;
  uint8_t *seq_octet = calloc(HPKE_AEAD_NN, 1);
  uint8_t *nonce = calloc(HPKE_AEAD_NN, 1);
  uint8_t *tag;
  if (!seq_octet || !nonce)
    {
      r = -1;
      goto out;
    }
  r = i2osp(ctx->seq_n, seq_octet, HPKE_AEAD_NN);
  if(r)
    goto out;

  xor_octet_arr(nonce, ctx->base_nonce, seq_octet, HPKE_AEAD_NN);

  if (ctx->s_ctx.aead == HPKE_AEAD_AES_128_GCM)
    {
      *pt_len = ct_len - GCM_DIGEST_SIZE;
      *pt = calloc(*pt_len, 1);
      tag = calloc(GCM_DIGEST_SIZE, 1);
      if (!*pt || !tag)
        {
          r = -1;
          goto out;
        }
      struct gcm_aes128_ctx c;
      gcm_aes128_set_key(&c, ctx->key);
      gcm_aes128_set_iv(&c, HPKE_AEAD_NN, nonce);
      gcm_aes128_update(&c, aad_len, aad);
      gcm_aes128_decrypt(&c, ct_len-GCM_DIGEST_SIZE, *pt, ct);
      gcm_aes128_digest(&c, GCM_DIGEST_SIZE, tag);
      r = memcmp(tag, ct+(ct_len-GCM_DIGEST_SIZE), GCM_DIGEST_SIZE);
    }
  else if (ctx->s_ctx.aead == HPKE_AEAD_AES_256_GCM)
    {
      *pt_len = ct_len - GCM_DIGEST_SIZE;
      *pt = calloc(*pt_len, 1);
      tag = calloc(GCM_DIGEST_SIZE, 1);
      if (!*pt || !tag)
        {
          r = -1;
          goto out;
        }
      struct gcm_aes256_ctx c;
      gcm_aes256_set_key(&c, ctx->key);
      gcm_aes256_set_iv(&c, HPKE_AEAD_NN, nonce);
      gcm_aes256_update(&c, aad_len, aad);
      gcm_aes256_decrypt(&c, ct_len-GCM_DIGEST_SIZE, *pt, ct);
      gcm_aes256_digest(&c, GCM_DIGEST_SIZE, tag);
      r = memcmp(tag, ct+(ct_len-GCM_DIGEST_SIZE), GCM_DIGEST_SIZE);
    }
  else if (ctx->s_ctx.aead == HPKE_AEAD_CHACHA20POLY1305)
    {
      *pt_len = ct_len - CHACHA_POLY1305_DIGEST_SIZE;
      *pt = calloc(*pt_len, 1);
      tag = calloc(CHACHA_POLY1305_DIGEST_SIZE, 1);
      if (!*pt || !tag)
        {
          r = -1;
          goto out;
        }
      struct chacha_poly1305_ctx c;
      chacha_poly1305_set_key(&c, ctx->key);
      chacha_poly1305_set_nonce(&c, nonce);
      chacha_poly1305_update(&c, aad_len, aad);
      chacha_poly1305_decrypt(&c, ct_len-CHACHA_POLY1305_DIGEST_SIZE, *pt, ct);
      chacha_poly1305_digest(&c, CHACHA_POLY1305_DIGEST_SIZE, tag);
      r = memcmp(tag,
                 ct+(ct_len-CHACHA_POLY1305_DIGEST_SIZE),
                 CHACHA_POLY1305_DIGEST_SIZE);
    }

  ++ctx->seq_n;

out:
  if(tag)
    free(tag);
  free(seq_octet);
  free(nonce);

  return r;
}

uint8_t *
export_secret (struct context *ctx, uint8_t *exporter_context,
               size_t exporter_context_len, size_t l)
{
  return labeled_expand_hpke(ctx->exporter_secret,
                             ctx->s_ctx.nh,
                             "sec",
                             exporter_context,
                             exporter_context_len,
                             l,
                             &(ctx->s_ctx));
}
