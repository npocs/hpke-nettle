/* TODO Insert license here */

#ifndef __HPKE_HPKE_H__
#define __HPKE_HPKE_H__

#include "hpke_types.h"
#include "dhkem.h"
#include "hkdf.h"

#include <stdbool.h>
#include <nettle/gcm.h>
#include <nettle/chacha-poly1305.h>

int
key_schedule (struct enc_ctx *e_ctx, struct suite_ctx *s_ctx,
              struct context *c, const uint8_t mode);

int
setup_base_s (uint8_t *pkr, size_t pkr_len, uint8_t *info, size_t info_len,
	      const uint16_t dhkem, const uint16_t hkdf, const uint16_t aead,
	      struct context *c);

int
setup_base_r (uint8_t *enc, size_t enc_len, uint8_t *pkr, size_t pkr_len,
              uint8_t *skr, size_t skr_len, uint8_t *info, size_t info_len,
	      const uint16_t dhkem, const uint16_t hkdf, const uint16_t aead,
              struct context *c);

int
setup_psk_s (uint8_t *pkr, size_t pkr_len, uint8_t *info, size_t info_len,
             uint8_t *psk, size_t psk_len, uint8_t *psk_id, size_t psk_id_len,
	     const uint16_t dhkem, const uint16_t hkdf, const uint16_t aead,
	     struct context *c);

int
setup_psk_r (uint8_t *enc, size_t enc_len, uint8_t *pkr, size_t pkr_len,
             uint8_t *skr, size_t skr_len, uint8_t *info, size_t info_len,
             uint8_t *psk, size_t psk_len, uint8_t *psk_id, size_t psk_id_len,
	     const uint16_t dhkem, const uint16_t hkdf, const uint16_t aead,
             struct context *c);

int
setup_auth_s (uint8_t *pkr, size_t pkr_len, 
              uint8_t *pks, size_t pks_len,
              uint8_t *sks, size_t sks_len,
              uint8_t *info, size_t info_len,
	      const uint16_t dhkem, const uint16_t hkdf, const uint16_t aead,
	      struct context *c);

int
setup_auth_r (uint8_t *enc, size_t enc_len, 
              uint8_t *pks, size_t pks_len,
              uint8_t *pkr, size_t pkr_len,
              uint8_t *skr, size_t skr_len,
              uint8_t *info, size_t info_len,
	      const uint16_t dhkem, const uint16_t hkdf, const uint16_t aead,
	      struct context *c);

int
setup_auth_psk_s (uint8_t *pkr, size_t pkr_len, 
                  uint8_t *pks, size_t pks_len,
                  uint8_t *sks, size_t sks_len,
                  uint8_t *psk, size_t psk_len,
                  uint8_t *psk_id, size_t psk_id_len,
                  uint8_t *info, size_t info_len,
	          const uint16_t dhkem, const uint16_t hkdf,
                  const uint16_t aead, struct context *c);

int
setup_auth_psk_r (uint8_t *enc, size_t enc_len, 
                  uint8_t *pks, size_t pks_len,
                  uint8_t *pkr, size_t pkr_len,
                  uint8_t *skr, size_t skr_len,
                  uint8_t *psk, size_t psk_len,
                  uint8_t *psk_id, size_t psk_id_len,
                  uint8_t *info, size_t info_len,
	          const uint16_t dhkem, const uint16_t hkdf,
                  const uint16_t aead, struct context *c);

#ifdef TEST
int
setup_base_s_testable (char *pkR, char *pkE, char *skE,
                       uint8_t *info, size_t info_len,
	               const uint16_t dhkem, const uint16_t hkdf, const uint16_t aead,
	               struct context *c);
int
setup_psk_s_testable (char *pkR, char *pkE, char *skE,
                      uint8_t *info, size_t info_len,
                      uint8_t *psk, size_t psk_len,
                      uint8_t *psk_id, size_t psk_id_len,
	              const uint16_t dhkem, const uint16_t hkdf,
                      const uint16_t aead, struct context *c);

int
setup_auth_s_testable (char *pkr, char *pks, char *sks,
                       char *pke, char *ske,
                       uint8_t *info, size_t info_len,
                       const uint16_t dhkem, const uint16_t hkdf, const uint16_t aead,
                       struct context *c);

int
setup_auth_psk_s_testable (char *pkr, char *pks, char *sks,
                           char *pke, char *ske,
                           uint8_t *psk, size_t psk_len,
                           uint8_t *psk_id, size_t psk_id_len,
                           uint8_t *info, size_t info_len,
                           const uint16_t dhkem, const uint16_t hkdf,
                           const uint16_t aead, struct context *c);
#endif

int
hpke_seal (struct context *c, uint8_t *aad, size_t aad_len,
      uint8_t *pt, size_t pt_len, uint8_t **ct, size_t *ct_len);

int
hpke_open (struct context *c, uint8_t *aad, size_t aad_len,
      uint8_t *ct, size_t ct_len, uint8_t **pt, size_t *pt_len);

uint8_t *
export_secret (struct context *ctx,
               uint8_t *exporter_context, size_t exporter_context_len,
               size_t l);

#endif /* __HPKE_HPKE_H__ */
