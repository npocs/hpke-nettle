/* FIXME Insert license here */

#include "hkdf.h"

// salt, label, ikm
uint8_t *
labeled_extract_hpke (uint8_t *salt, size_t salt_len, char *label,
                      uint8_t *ikm, size_t ikm_len, struct suite_ctx *s_ctx)
{
  uint8_t *dst;
  uint8_t *labeled_ikm;
  size_t labeled_ikm_offset = HPKE_RFC_NUM_LEN;

  const size_t label_len = strlen(label);
  const size_t labeled_ikm_len = HPKE_RFC_NUM_LEN + HPKE_HPKE_SUITE_ID_LEN
                                 + label_len + ikm_len;

  if (!(labeled_ikm = calloc(labeled_ikm_len, 1)))
    return NULL;
  memcpy(labeled_ikm, HPKE_RFC_NUM, HPKE_RFC_NUM_LEN);

  uint8_t *id = calloc(2, 1);
  if (!id)
    {
      free(labeled_ikm);
      return NULL;
    }
  if (i2osp(s_ctx->dhkem, id, 2))
    {
      free(labeled_ikm);
      free(id);
      return NULL;
    }

  memcpy(labeled_ikm + labeled_ikm_offset, "HPKE", 4);
  labeled_ikm_offset += 4;
  memcpy(labeled_ikm + labeled_ikm_offset, id, HPKE_KEM_ID_LEN);
  labeled_ikm_offset += HPKE_KEM_ID_LEN;
  if (i2osp(s_ctx->hkdf, id, 2))
    {
      free(labeled_ikm);
      free(id);
      return NULL;
    }
  memcpy(labeled_ikm + labeled_ikm_offset, id, HPKE_KDF_ID_LEN);
  labeled_ikm_offset += HPKE_KDF_ID_LEN;
  if (i2osp(s_ctx->aead, id, 2))
    {
      free(labeled_ikm);
      free(id);
      return NULL;
    }
  memcpy(labeled_ikm + labeled_ikm_offset, id, HPKE_AEAD_ID_LEN);
  labeled_ikm_offset += HPKE_AEAD_ID_LEN;

  free(id);
  memcpy(labeled_ikm + labeled_ikm_offset, label, label_len);
  labeled_ikm_offset += label_len;
  memcpy(labeled_ikm + labeled_ikm_offset, ikm, ikm_len);

  if (s_ctx->hkdf == HPKE_HKDF_SHA256)
    {
      struct hmac_sha256_ctx ctx;
      hmac_sha256_set_key(&ctx, salt_len, salt);
      s_ctx->digest_size = SHA256_DIGEST_SIZE;
      if (!(dst = calloc(SHA256_DIGEST_SIZE, 1)))
        {
          free(labeled_ikm);
          return NULL;
        }
      hkdf_extract(&ctx,
                   (nettle_hash_update_func*) hmac_sha256_update,
                   (nettle_hash_digest_func*) hmac_sha256_digest,
                   SHA256_DIGEST_SIZE,
                   labeled_ikm_len,
                   labeled_ikm,
                   dst);
    }
  else
    {
      struct hmac_sha512_ctx ctx;
      hmac_sha512_set_key(&ctx, salt_len, salt);
      if (s_ctx->hkdf == HPKE_HKDF_SHA384)
        {
          s_ctx->digest_size = SHA384_DIGEST_SIZE;
          if (!(dst = calloc(SHA384_DIGEST_SIZE, 1)))
            {
              free(labeled_ikm);
              return NULL;
            }
          hkdf_extract(&ctx,
                       (nettle_hash_update_func*) hmac_sha384_update,
                       (nettle_hash_digest_func*) hmac_sha384_digest,
                       SHA384_DIGEST_SIZE,
                       labeled_ikm_len,
                       labeled_ikm,
                       dst);
        }
      else
        {
          s_ctx->digest_size = SHA512_DIGEST_SIZE;
          if (!(dst = calloc(SHA512_DIGEST_SIZE, 1)))
            {
              free(labeled_ikm);
              return NULL;
            }
          hkdf_extract(&ctx,
                       (nettle_hash_update_func*) hmac_sha512_update,
                       (nettle_hash_digest_func*) hmac_sha512_digest,
                       SHA512_DIGEST_SIZE,
                       labeled_ikm_len,
                       labeled_ikm,
                       dst);
        }
    }

  free(labeled_ikm);

  return dst;
}

// prk, label, info, L
uint8_t *
labeled_expand_hpke (uint8_t *prk, size_t prk_len, char *label, uint8_t *info,
                     size_t info_len, uint16_t l, struct suite_ctx *s_ctx)
{
  uint8_t *dst;
  uint8_t *labeled_info;
  size_t labeled_info_offset = 2;

  const size_t label_len = strlen(label);
  const size_t labeled_info_len = 2 + HPKE_RFC_NUM_LEN + HPKE_HPKE_SUITE_ID_LEN
                                  + label_len + info_len;

  if (!(dst = calloc(l, 1)))
    return NULL;
  if (!(labeled_info = calloc(labeled_info_len, 1)))
    {
      free(dst);
      return NULL;
    }

  uint8_t *id = calloc(2, 1);
  if (!id)
    {
      free(dst);
      free(labeled_info);
      return NULL;
    }
  if (i2osp(l, id, 2))
    {
      free(dst);
      free(labeled_info);
      free(id);
      return NULL;
    }
  memcpy(labeled_info, id, 2);
  memcpy(labeled_info + labeled_info_offset, HPKE_RFC_NUM, HPKE_RFC_NUM_LEN);
  labeled_info_offset += HPKE_RFC_NUM_LEN;
  if (i2osp(s_ctx->dhkem, id, 2))
    {
      free(dst);
      free(labeled_info);
      free(id);
      return NULL;
    }

  memcpy(labeled_info + labeled_info_offset, "HPKE", 4);
  labeled_info_offset += 4;
  memcpy(labeled_info + labeled_info_offset, id, HPKE_KEM_ID_LEN);
  labeled_info_offset += HPKE_KEM_ID_LEN;
  if (i2osp(s_ctx->hkdf, id, 2))
    {
      free(dst);
      free(labeled_info);
      free(id);
      return NULL;
    }
  memcpy(labeled_info + labeled_info_offset, id, HPKE_KDF_ID_LEN);
  labeled_info_offset += HPKE_KDF_ID_LEN;
  if (i2osp(s_ctx->aead, id, 2))
    {
      free(dst);
      free(labeled_info);
      free(id);
      return NULL;
    }
  memcpy(labeled_info + labeled_info_offset, id, HPKE_AEAD_ID_LEN);
  labeled_info_offset += HPKE_AEAD_ID_LEN;

  free(id);
  memcpy(labeled_info + labeled_info_offset, label, label_len);
  labeled_info_offset += label_len;
  memcpy(labeled_info + labeled_info_offset, info, info_len);

  if (s_ctx->hkdf == HPKE_HKDF_SHA256)
    {
      struct hmac_sha256_ctx ctx;
      hmac_sha256_set_key(&ctx, prk_len, prk);
      hkdf_expand(&ctx,
                  (nettle_hash_update_func*) hmac_sha256_update,
                  (nettle_hash_digest_func*) hmac_sha256_digest,
                  SHA256_DIGEST_SIZE,
                  labeled_info_len,
                  labeled_info,
                  l,
                  dst);
    }
  else
    {
      struct hmac_sha512_ctx ctx;
      hmac_sha512_set_key(&ctx, prk_len, prk);
      if (s_ctx->hkdf == HPKE_HKDF_SHA384)
        {
          hkdf_expand(&ctx,
                      (nettle_hash_update_func*) hmac_sha384_update,
                      (nettle_hash_digest_func*) hmac_sha384_digest,
                      SHA384_DIGEST_SIZE,
                      labeled_info_len,
                      labeled_info,
                      l,
                      dst);
        }
      else
        {
          hkdf_expand(&ctx,
                      (nettle_hash_update_func*) hmac_sha512_update,
                      (nettle_hash_digest_func*) hmac_sha512_digest,
                      SHA512_DIGEST_SIZE,
                      labeled_info_len,
                      labeled_info,
                      l,
                      dst);
        }
    }

  free(labeled_info);

  return dst;
}
