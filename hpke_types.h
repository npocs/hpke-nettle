/* FIXME Insert license here */

#ifndef __HPKE_NETTLE_TYPES__
#define __HPKE_NETTLE_TYPES__

#include <stdint.h>
#include <string.h>
#include <nettle/hmac.h>
#include <nettle/ecc.h>
#include <nettle/ecc-curve.h>
#include <nettle/curve25519.h>
#include <nettle/curve448.h>
#include <stdlib.h>
#include <math.h>

#define HPKE_RFC_NUM			"HPKE-v1" //"RFCXXXX"
#define HPKE_RFC_NUM_LEN		7
#define HPKE_KEM_SUITE_ID_LEN		5
#define HPKE_HPKE_SUITE_ID_LEN		10
#define HPKE_KEM_ID_LEN			2
#define HPKE_KDF_ID_LEN			2
#define HPKE_AEAD_ID_LEN		2

#define HPKE_MODE_BASE			0x00
#define HPKE_MODE_PSK			0x01
#define HPKE_MODE_AUTH			0x02
#define HPKE_MODE_AUTH_PSK		0x03

#define HPKE_DHKEM_P256			0x0010
#define HPKE_DHKEM_P384			0x0011
#define HPKE_DHKEM_P521			0x0012
#define HPKE_DHKEM_X25519		0x0020
#define HPKE_DHKEM_X448			0x0021

#define HPKE_HKDF_SHA256		0x0001
#define HPKE_HKDF_SHA384		0x0002
#define HPKE_HKDF_SHA512		0x0003

#define HPKE_AEAD_AES_128_GCM		0x0001
#define HPKE_AEAD_AES_256_GCM		0x0002
#define HPKE_AEAD_CHACHA20POLY1305	0x0003
#define HPKE_AEAD_EXPORT_ONLY		0xffff

#define HPKE_AEAD_AES_128_GCM_NK	16
#define HPKE_AEAD_AES_256_GCM_NK	32
#define HPKE_AEAD_CHACHA20POLY1305_NK	32
#define HPKE_AEAD_NN			12

#define HPKE_EAE_PRK_LABEL		"eae_prk"
#define HPKE_SHARED_S_LABEL		"shared_secret"

#define HPKE_KEYS_S			0
#define HPKE_KEYS_R			1


#define HPKE_MAX_LEN_HKDF_SHA256_PSK			2305843009213693864
#define HPKE_MAX_LEN_HKDF_SHA256_PSK_ID			2305843009213693859
#define HPKE_MAX_LEN_HKDF_SHA256_INFO			2305843009213693861
#define HPKE_MAX_LEN_HKDF_SHA256_EXPORTER_CONTEXT	2305843009213693832

#define HPKE_MAX_LEN_HKDF_SHA384_PSK			42535295865117307932921825928971026280
#define HPKE_MAX_LEN_HKDF_SHA384_PSK_ID			42535295865117307932921825928971026275

#define HPKE_ECC_ORDER_P256				"115792089210356248762697446949407573529996955224135760342422259061068512044369"
#define HPKE_ECC_ORDER_P384				"39402006196394479212279040100143613805079739270465446667946905279627659399113263569398956308152294913554433653942643"
#define HPKE_ECC_ORDER_P521				"6864797660130609714981900799081393217269435300143305409394463459185543183397655394245057746333217197532963996371363321113864768612440380340372808892707005449"

struct suite_ctx {
	uint16_t dhkem;
	uint16_t hkdf;
	uint16_t aead;
	size_t enc_len;
	uint8_t nsk;
	size_t digest_size;
	uint8_t Nsecret;
	uint8_t nh;
	uint8_t nk;
	uint8_t *shared_secret;
	uint8_t *enc;
	const char *ecc_order;
	const struct ecc_curve *ecc;
};

struct enc_ctx {
	uint8_t mode;
	uint8_t *info;
	size_t info_len;
	uint8_t *psk;
	size_t psk_len;
	uint8_t *psk_id;
	size_t psk_id_len;
};

struct context {
	uint8_t *key;
	uint8_t *base_nonce;
	size_t seq_n;
	uint8_t *exporter_secret;
	struct suite_ctx s_ctx;
};

void
suite_init (struct suite_ctx *c, const uint16_t dhkem,
	    const uint16_t hkdf, const uint16_t aead);
void
suite_clear (struct suite_ctx *c);

int
enc_init (struct enc_ctx *e, const uint8_t mode,
	  uint8_t *info, size_t info_len, uint8_t *psk, size_t psk_len,
	  uint8_t *psk_id, size_t psk_id_len);
void
enc_clear (struct enc_ctx *e);

int
context_init (struct context *c, uint8_t *key, uint8_t *base_nonce,
              uint8_t *exporter_secret, struct suite_ctx *s_ctx);
void
context_clear (struct context *c);

int
i2osp (size_t n, uint8_t *s, const uint8_t l);

void
os2ip (uint8_t *s, size_t *n, const uint8_t l);

int
ecc_point_to_octet (uint8_t **s, size_t *s_len, struct ecc_point *p,
                    struct suite_ctx *s_ctx);

int
octet_to_ecc_point (struct ecc_point *p, uint8_t *s, size_t s_len,
                    struct suite_ctx *s_ctx);

int
octet_to_hex_string (char **dst, uint8_t *src, size_t src_len);

int
hex_string_to_octet (uint8_t **dst, size_t *dst_len, char *src);

void
xor_octet_arr (uint8_t *dst, uint8_t *src0, uint8_t *src1, size_t len);

#endif /* __HPKE_NETTLE_TYPES__ */
