/* TODO Insert license here */

#include "dhkem.h"

/* pkR should be a hex string of an octet string representing the
 * point on the ec */
int
encap_auth (uint8_t *pkr, size_t pkr_len, uint8_t *pks, size_t pks_len,
            uint8_t *sks, size_t sks_len, struct suite_ctx *s_ctx)
{
  uint8_t *enc, *dh_ser, *kem_context;
  size_t dh_ser_len, enc_len;
  uint8_t entropy[s_ctx->nsk];

  if (getrandom((void*)entropy, s_ctx->nsk, 0) < s_ctx->nsk)
    return -1;

  if (s_ctx->dhkem == HPKE_DHKEM_P256
      || s_ctx->dhkem == HPKE_DHKEM_P384
      || s_ctx->dhkem == HPKE_DHKEM_P521)
    {
      char *sksm;
      struct ecc_point p, dh_e, dh_s, pR;
      struct ecc_scalar key_e, key_s;
      mpz_t x, z;

      ecc_point_init(&pR, s_ctx->ecc);
      octet_to_ecc_point(&pR, pkr, pkr_len, s_ctx);

      ecc_point_init(&p, s_ctx->ecc);
      ecc_scalar_init(&key_e, s_ctx->ecc);

      derive_key_pair_p(entropy, s_ctx->nsk, s_ctx, &key_e, &p);

      ecc_point_init(&dh_e, s_ctx->ecc);
      ecc_point_mul(&dh_e, &key_e, &pR);

      octet_to_hex_string(&sksm, sks, sks_len);
      mpz_init_set_str(z, sksm, 16);
      ecc_scalar_set(&key_s, z);
      free(sksm);
      mpz_clear(z);

      ecc_point_init(&dh_s, s_ctx->ecc);
      ecc_point_mul(&dh_s, &key_s, &pR);

      mpz_init(x);
      ecc_point_get(&dh_e, x, NULL);
      dh_ser_len = ((ecc_bit_size(s_ctx->ecc) + 7) / 8) * 2;
      dh_ser = calloc(dh_ser_len, 1);
      if (!dh_ser)
        {
          mpz_clear(x);
          ecc_point_clear(&p);
          ecc_point_clear(&pR);
          ecc_point_clear(&dh_e);
          ecc_point_clear(&dh_s);
          ecc_scalar_clear(&key_e);
          return -1;
        }
      size_t one_dh_len = (dh_ser_len / 2);
      nettle_mpz_get_str_256(one_dh_len, dh_ser, x);
      mpz_clear(x);

      mpz_init(x);
      ecc_point_get(&dh_s, x, NULL);
      nettle_mpz_get_str_256(one_dh_len, (dh_ser + one_dh_len), x);
      mpz_clear(x);

      ecc_point_to_octet(&enc, &enc_len, &p, s_ctx);

      ecc_point_clear(&p);
      ecc_point_clear(&pR);
      ecc_point_clear(&dh_e);
      ecc_point_clear(&dh_s);
      ecc_scalar_clear(&key_e);
    }
  else
    {
      uint8_t *sk;

      if (derive_key_pair_x(entropy, s_ctx->nsk, s_ctx, &sk, &enc))
        return -1;
      enc_len = s_ctx->nsk;

      dh_ser_len = s_ctx->nsk * 2;
      dh_ser = calloc(dh_ser_len, 1);
      if (!dh_ser)
        {
          dh_ser_len = 0;
          free(sk);
          free(enc);
          return -1;
        }

      if (s_ctx->dhkem == HPKE_DHKEM_X25519)
        {
          curve25519_mul(dh_ser, sk, pkr);
          curve25519_mul((dh_ser + s_ctx->nsk), sks, pkr);
        }
      else if (s_ctx->dhkem == HPKE_DHKEM_X448)
        {
          curve448_mul(dh_ser, sk, pkr);
          curve448_mul((dh_ser + s_ctx->nsk), sks, pkr);
        }

      free(sk);
    }

  kem_context = calloc(enc_len + pkr_len + pks_len, 1);
  if (!kem_context)
    {
      free(enc);
      free(dh_ser);
      return -1;
    }
  memcpy(kem_context, enc, enc_len);
  memcpy(kem_context + enc_len, pkr, pkr_len);
  memcpy(kem_context + enc_len + pkr_len, pks, pks_len);

  s_ctx->shared_secret = extract_and_expand_kem(dh_ser,
                                                dh_ser_len,
                                                kem_context,
                                                enc_len+pkr_len+pks_len,
                                                s_ctx);
  if (!s_ctx->shared_secret)
    {
      free(enc);
      free(dh_ser);
      free(kem_context);
      return -1;
    }
  s_ctx->enc = enc;
  s_ctx->enc_len = enc_len;

  free(dh_ser);
  free(kem_context);

  return 0;
}

int
decap_auth (uint8_t *enc, size_t enc_len, uint8_t *pks, size_t pks_len,
            uint8_t *pkr, size_t pkr_len, uint8_t *skr, size_t skr_len,
            struct suite_ctx *s_ctx)
{
  uint8_t *kem_context, *dh_ser;
  size_t dh_ser_len;

  if (s_ctx->dhkem == HPKE_DHKEM_P256
      || s_ctx->dhkem == HPKE_DHKEM_P384
      || s_ctx->dhkem == HPKE_DHKEM_P521)
    {
      struct ecc_point pkE, pkS, dh_e, dh_s;
      struct ecc_scalar skR;
      char *skrm;
      mpz_t x, z;
      ecc_point_init(&pkE, s_ctx->ecc);
      ecc_point_init(&pkS, s_ctx->ecc);
      ecc_point_init(&dh_e, s_ctx->ecc);
      ecc_point_init(&dh_s, s_ctx->ecc);
      ecc_scalar_init(&skR, s_ctx->ecc);

      octet_to_hex_string(&skrm, skr, skr_len);
      mpz_init_set_str(z, skrm, 16);
      ecc_scalar_set(&skR, z);

      octet_to_ecc_point(&pkE, enc, enc_len, s_ctx);
      octet_to_ecc_point(&pkS, pks, pks_len, s_ctx);

      ecc_point_mul(&dh_e, &skR, &pkE);
      ecc_point_mul(&dh_s, &skR, &pkS);

      mpz_init(x);
      ecc_point_get(&dh_e, x, NULL);
      dh_ser_len = ((ecc_bit_size(s_ctx->ecc) + 7) / 8) * 2;
      dh_ser = calloc(dh_ser_len, 1);
      if (!dh_ser)
        {
          mpz_clear(x);
          ecc_point_clear(&pkE);
          ecc_point_clear(&pkS);
          ecc_point_clear(&dh_e);
          ecc_point_clear(&dh_s);
          ecc_scalar_clear(&skR);
          free(skrm);
          mpz_clear(z);
          return -1;
        }
      size_t one_dh_len = (dh_ser_len / 2);
      nettle_mpz_get_str_256(one_dh_len, dh_ser, x);
      mpz_clear(x);

      mpz_init(x);
      ecc_point_get(&dh_s, x, NULL);
      nettle_mpz_get_str_256(one_dh_len, dh_ser + one_dh_len, x);
      mpz_clear(x);

      ecc_point_clear(&pkE);
      ecc_point_clear(&pkS);
      ecc_point_clear(&dh_e);
      ecc_point_clear(&dh_s);
      ecc_scalar_clear(&skR);
      free(skrm);
      mpz_clear(z);
    }
  else if (s_ctx->dhkem == HPKE_DHKEM_X25519)
    {
      dh_ser_len = s_ctx->nsk * 2;
      dh_ser = calloc(dh_ser_len, 1);
      if (!dh_ser)
        return -1;
      curve25519_mul(dh_ser, skr, enc);
      curve25519_mul(dh_ser + s_ctx->nsk, skr, pks);
    }
  else if (s_ctx->dhkem == HPKE_DHKEM_X448)
    {
      dh_ser_len = s_ctx->nsk * 2;
      dh_ser = calloc(dh_ser_len, 1);
      if (!dh_ser)
        return -1;
      curve448_mul(dh_ser, skr, enc);
      curve448_mul(dh_ser + s_ctx->nsk, skr, pks);
    }

  kem_context = calloc(enc_len + pkr_len + pks_len, 1);
  if (!kem_context)
    {
      free(dh_ser);
      return -1;
    }
  memcpy(kem_context, enc, enc_len);
  memcpy(kem_context + enc_len, pkr, pkr_len);
  memcpy(kem_context + enc_len + pkr_len, pks, pks_len);

  int r = 0;
  s_ctx->shared_secret = extract_and_expand_kem(dh_ser,
                                                dh_ser_len,
                                                kem_context,
                                                enc_len+pkr_len+pks_len,
                                                s_ctx);
  if (!s_ctx->shared_secret)
    r = -1;

  free(dh_ser);
  free(kem_context);

  return r;
}

#ifdef TEST
int
encap_auth_testable (char *pkR, char *pkE, char *skE, char *pkS, char *skS,
                     struct suite_ctx *s_ctx)
{
  uint8_t *enc, *pkRm, *dh_ser, *kem_context;
  size_t dh_ser_len, enc_len, pkRm_len;

  hex_string_to_octet(&pkRm, &pkRm_len, pkR);

  if (s_ctx->dhkem == HPKE_DHKEM_P256
      || s_ctx->dhkem == HPKE_DHKEM_P384
      || s_ctx->dhkem == HPKE_DHKEM_P521)
    {
      uint8_t *pke_o, *ske_o;
      size_t pke_o_len, ske_o_len;
      struct ecc_point p, dh_e, dh_s, pR;
      struct ecc_scalar key_e, key_s;
      mpz_t x, z;

      ecc_point_init(&pR, s_ctx->ecc);
      octet_to_ecc_point(&pR, pkRm, pkRm_len, s_ctx);

      ecc_point_init(&p, s_ctx->ecc);
      ecc_scalar_init(&key_e, s_ctx->ecc);

      mpz_init(z);

      hex_string_to_octet(&pke_o, &pke_o_len, pkE);
      hex_string_to_octet(&ske_o, &ske_o_len, skE);
      octet_to_ecc_point(&p, pke_o, pke_o_len, s_ctx);
      nettle_mpz_set_str_256_u(z, ske_o_len, ske_o);
      ecc_scalar_set(&key_e, z);

      mpz_clear(z);

      ecc_point_init(&dh_e, s_ctx->ecc);
      ecc_point_mul(&dh_e, &key_e, &pR);

      ecc_scalar_init(&key_s, s_ctx->ecc);
      mpz_init_set_str(z, skS, 16);
      ecc_scalar_set(&key_s, z);
      mpz_clear(z);

      ecc_point_init(&dh_s, s_ctx->ecc);
      ecc_point_mul(&dh_s, &key_s, &pR);

      mpz_init(x);
      ecc_point_get(&dh_e, x, NULL);
      dh_ser_len = ((ecc_bit_size(s_ctx->ecc) + 7) / 8) * 2;
      size_t one_dh_len = dh_ser_len / 2;
      dh_ser = calloc(dh_ser_len, 1);
      nettle_mpz_get_str_256(one_dh_len, dh_ser, x);
      mpz_clear(x);

      mpz_init(x);
      ecc_point_get(&dh_s, x, NULL);
      nettle_mpz_get_str_256(one_dh_len, dh_ser + one_dh_len, x);
      mpz_clear(x);

      ecc_point_to_octet(&enc, &enc_len, &p, s_ctx);

      free(pke_o);
      free(ske_o);
      ecc_point_clear(&p);
      ecc_point_clear(&pR);
      ecc_point_clear(&dh_e);
      ecc_point_clear(&dh_s);
      ecc_scalar_clear(&key_e);
      ecc_scalar_clear(&key_s);
    }
  else
    {
      uint8_t *sk, *sksm;
      size_t sk_len, sksm_len;

      hex_string_to_octet(&enc, &enc_len, pkE);
      hex_string_to_octet(&sk, &sk_len, skE);
      hex_string_to_octet(&sksm, &sksm_len, skS);

      dh_ser_len = s_ctx->nsk * 2;
      dh_ser = calloc(dh_ser_len, 1);
      if (s_ctx->dhkem == HPKE_DHKEM_X25519)
        {
          curve25519_mul(dh_ser, sk, pkRm);
          curve25519_mul(dh_ser + s_ctx->nsk, sksm, pkRm);
        }
      else if (s_ctx->dhkem == HPKE_DHKEM_X448)
        {
          curve448_mul(dh_ser, sk, pkRm);
          curve448_mul(dh_ser + s_ctx->nsk, sksm, pkRm);
        }
      free(sk);
      free(sksm);
    }

  uint8_t *pksm;
  size_t pksm_len;

  hex_string_to_octet(&pksm, &pksm_len, pkS);

  kem_context = calloc(enc_len + pkRm_len + pksm_len, 1);
  memcpy(kem_context, enc, enc_len);
  memcpy(kem_context + enc_len, pkRm, pkRm_len);
  memcpy(kem_context + enc_len + pkRm_len, pksm, pksm_len);

  s_ctx->shared_secret = extract_and_expand_kem(dh_ser,
                                                dh_ser_len,
                                                kem_context,
                                                enc_len+pkRm_len+pksm_len,
                                                s_ctx);
  s_ctx->enc = enc;
  s_ctx->enc_len = enc_len;

  free(dh_ser);
  free(pkRm);
  free(pksm);
  free(kem_context);

  return 0;
}
#endif /* TEST */
